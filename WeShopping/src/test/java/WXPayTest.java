import java.util.HashMap;

import org.apache.solr.common.util.Hash;
import org.junit.Test;

import com.wxpay.gzdcdata.pay.WXPay;
import com.wxpay.gzdcdata.pay.WXPayConfig;
import com.wxpay.gzdcdata.pay.impl.WXPayConfigImpl;

public class WXPayTest {
	//查询订单
	//@Test
	public void QueryOrder() throws Exception{
	     WXPayConfig config = WXPayConfigImpl.getInstance();
		 WXPay  wxpay = new WXPay(config);

		 HashMap<String, String> reqData = new HashMap<String,String>();
		 reqData.put("appid", "wx3a59d3c5f923218e");
		 reqData.put("mch_id", "1481627572");
		 reqData.put("nonce_str", "ibuaiVcKdpRxkhJA");
		 reqData.put("transaction_id", "BKVsq7jVeyAX4bpw");
		 reqData.put("out_trade_no", "201803061317");

        System.out.println( wxpay.orderQuery(reqData));
		String str="<xml>"
				+ "<appid>wx3a59d3c5f923218e</appid>"
				+ "<mch_id>1481627572</mch_id>"
				+ "<nonce_str>BKVsq7jVeyAX4bpw</nonce_str>"
				+ "<transaction_id>wx10134735252719a1b405035d4086091520</transaction_id>"
				+ "<sign>CB1EFE565ECDB29607B1C62F59B556E9EFF11A34990056006056B6CC25452718</sign>"
				+ "</xml>";

	}
}
