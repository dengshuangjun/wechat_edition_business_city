package com.wxpay.gzdcdata.pay.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxpay.gzdcdata.pay.IWXPayDomain;
import com.wxpay.gzdcdata.pay.WXPay;
import com.wxpay.gzdcdata.pay.WXPayConfig;
/**
 * 微信支付基本配置
 * @author dsj
 *
 */
/*@Component("wXPayConfig")*/
public class WXPayConfigImpl extends WXPayConfig{

    private byte[] certData;
    private static WXPayConfigImpl INSTANCE;
    protected Logger logger = LoggerFactory.getLogger(WXPayConfig.class);
    private WXPayConfigImpl() throws Exception{
    	String path=System.getProperty("user.dir");
        String certPath = path+File.separator+"target"+File.separator+"classes"+File.separator+"wxpay"+File.separator+"apiclient_cert.p12";
        if(certPath.indexOf("version")>0){
        	//TODO 这个证书的地址暂时是固定写了
        	path="/home/WxShop/apache-tomcat-7.0.76-WxShop-9323/webapps/WxShop/WEB-INF/classes/wxpay/";
            certPath = path+"apiclient_cert.p12";
        }
        logger.info("=====>"+certPath);
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    public static WXPayConfigImpl getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (WXPayConfigImpl.class) {
                if (INSTANCE == null) {
                    INSTANCE = new WXPayConfigImpl();
                }
            }
        }
        return INSTANCE;
    }

    public String getAppID() {
        return "wx3a59d3c5f923218e";
    }

    public String getMchID() {
        return "1481627572";
    }

    public String getKey() {
        return "jmcMnvjlod9dl0TYI6y5Ih3SjVdUt1TW";
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }


    public int getHttpConnectTimeoutMs() {
        return 4000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWXPayDomain getWXPayDomain() {
        return WXPayDomainSimpleImpl.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }

  public static void main(String[] args) {
		String path=System.getProperty("user.dir");
        String certPath = path+File.separator+"target"+File.separator+"classes"+File.separator+"wxpay"+File.separator+"apiclient_cert.p12";
        System.out.println("=====>"+certPath);
        File file = new File(certPath);
        try {
			InputStream certStream = new FileInputStream(file);
			System.out.println("=====>"+certStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }

}
