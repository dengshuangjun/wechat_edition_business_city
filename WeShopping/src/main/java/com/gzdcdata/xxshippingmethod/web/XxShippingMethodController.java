package com.gzdcdata.xxshippingmethod.web;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.xxshippingmethod.entity.XxShippingMethod;
import com.gzdcdata.xxshippingmethod.service.XxShippingMethodService;
/**
 * 配送方式控制器
 * @author dsj
 *
 */
@Controller
@RequestMapping("/xxshippingmethod")
public class XxShippingMethodController extends ApiController{

	@Resource(name="xxShippingMethodService")
	private XxShippingMethodService xxShippingMethodService;
	/**
	 * 获取默认配送方式信息
	 * @return
	 */
	@GetMapping("/getDefaultInfo")
	private ResponseEntity<ResponseData> getDefaultInfo(){
		try {
			XxShippingMethod result = xxShippingMethodService.getDefaultInfo();
			return new ResponseEntity<ResponseData>(success(result),HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("查询错误！"),HttpStatus.BAD_REQUEST);
	}

}
