package com.gzdcdata.xxshippingmethod.dao;

import com.gzdcdata.xxshippingmethod.entity.XxShippingMethod;

public interface XxShippingMethodDao {
	/**
	 * 查询默认配送方式
	 * @return
	 */
	XxShippingMethod getDefaultInfo();

}
