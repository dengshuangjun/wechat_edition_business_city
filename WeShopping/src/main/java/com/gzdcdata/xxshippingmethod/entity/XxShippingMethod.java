package com.gzdcdata.xxshippingmethod.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;
/**
 * 物流配送实体类
 * @author dsj
 *
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxShippingMethod {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date	;//创建日期
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date  modify_date	;//修改日期
	private Integer orders;//	排序
	private Long continue_price;//	续重价格
	private Integer continue_weight;//	续重量
	private String description;//	介绍
	private Long first_price;//	首重价格
	private Integer first_weight;//	首重量
	private String icon;//	图标
	private String name;//	名称
	private Long default_delivery_corp;//	默认物流公司
	//非数据库字段  物流公司名字
	private String realname;

}
