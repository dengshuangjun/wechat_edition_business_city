package com.gzdcdata.xxshippingmethod.service;

import com.gzdcdata.xxshippingmethod.entity.XxShippingMethod;

public interface XxShippingMethodService {

	/**
	 * 获取默认配送方式信息
	 * @return
	 */
	XxShippingMethod getDefaultInfo() throws Exception;

}
