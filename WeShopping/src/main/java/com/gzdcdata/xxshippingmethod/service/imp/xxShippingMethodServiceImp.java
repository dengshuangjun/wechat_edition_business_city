package com.gzdcdata.xxshippingmethod.service.imp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxshippingmethod.dao.XxShippingMethodDao;
import com.gzdcdata.xxshippingmethod.entity.XxShippingMethod;
import com.gzdcdata.xxshippingmethod.service.XxShippingMethodService;

/**
 * 物流配送服务层
 * @author dsj
 *
 */
@Service("xxShippingMethodService")
public class xxShippingMethodServiceImp implements XxShippingMethodService {

	@Resource(name="xxShippingMethodDao")
	private XxShippingMethodDao xxShippingMethodDao;

	@Override
	public XxShippingMethod getDefaultInfo() throws Exception{
		return xxShippingMethodDao.getDefaultInfo();
	}
}
