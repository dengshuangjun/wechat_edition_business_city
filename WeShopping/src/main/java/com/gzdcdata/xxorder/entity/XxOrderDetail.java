package com.gzdcdata.xxorder.entity;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gzdcdata.xxorderitem.entity.XxOrderItem;
import com.gzdcdata.xxpayment.entity.XxPayment;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxOrderDetail {
	/**
	 * xxorder对象字段
	 */
	private Long id;
	//运费
	private Double freight;
	//订单号
	private String sn;
	private List<XxOrderItem> xxOrderItems;

	private Double amount;

	private String memo;

	private XxPayment xxPayment;

	private String address;

	private String phone;

	private String area_name;

	private String consignee;

	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	private Date create_date;

	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	private Date expire;
	private Integer order_status;
	/**
	 * 获取剩下多少时间订单闭关
	 * @return
	 */
	public String getExpandTime(){
		try {
			Date nowTime = new Date();
			Long diff=expire.getTime()- nowTime.getTime();
			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
			long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
			return days+"天"+hours+"时"+minutes+"分";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
