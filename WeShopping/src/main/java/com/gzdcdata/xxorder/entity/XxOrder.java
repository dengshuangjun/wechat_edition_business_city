package com.gzdcdata.xxorder.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

/**
 * 订单实体
 * @author dsj
 *
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxOrder {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private String address;
	private Long amount_paid;
	private String area_name;
	private String consignee;
	private Long coupon_discount;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date expire;
	private Long fee;
	private Long freight;
	private String invoice_title;
	private Boolean is_allocated_stock;
	private Boolean is_invoice;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date lock_expire;
	private String memo;
	private Long offset_amount;
	private Integer order_status;
	private String payment_method_name;
	private Integer payment_status;
	private String phone;
	private Long point;
	private String promotion;
	private Long promotion_discount;
	private String shipping_method_name;
	private Integer shipping_status;
	private String sn;
	private Long tax;
	private String zip_code;
	private Long area;
	private Long coupon_code;
	private Long member;
	private Long operator;
	private Long payment_method;
	private Long shipping_method;
	private Long merch_product;
	private Long discount;
	private Boolean is_hide;
	private Long usepoint;
	private Long suborder;
}
