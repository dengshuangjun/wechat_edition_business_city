package com.gzdcdata.xxorder.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gzdcdata.xxorder.entity.XxOrder;
import com.gzdcdata.xxorder.entity.XxOrderDetail;

public interface XxOrderDao {

	void save(XxOrder xxOrder);

	/**
	 * 查询字段见实体类
	 * @param order_status
	 * @param userId
	 * @return
	 */
	List<XxOrderDetail> listByStatusAndUser(@Param("order_status")Integer order_status,@Param("userId") Long userId);
	/**
	 * 获取指定状态订单的数目
	 * @param memberId
	 * @param order_status
	 * @return
	 */
	int countByStatusMember(@Param("memberId")String memberId,@Param("order_status") String order_status);
	/**
	 * 通过订单号查询订单id
	 * @param sn
	 * @return
	 */
	XxOrder selectOrderBySn(String sn);
	/**
	 * 通过订单ID查询订单
	 * @param id
	 * @return
	 */
	XxOrder selectOrderById(@Param("id")Long id);
	/**
	 * 更新订单状态
	 * @param memberId 会员编号
	 * @param sn 订单号
	 * @param status 状态
	 * @return
	 */
	int updateOrderBySn(String memberId, String sn,Integer status);
	/**
	 * 查询订单信息
	 * @param sn
	 * @param memberId
	 * @return
	 */
	XxOrderDetail selectOrderBySnAndMember(@Param("sn")String sn, @Param("memberId")String memberId);
	/**
	 * 通过支付id更新订单支付状态
	 * @param paymentId
	 * @param order_status
	 */
	void updateStatusByPaymentId(String paymentId, int order_status);

}
