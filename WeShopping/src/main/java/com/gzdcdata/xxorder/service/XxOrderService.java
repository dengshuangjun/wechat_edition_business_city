package com.gzdcdata.xxorder.service;

import java.util.List;
import java.util.Map;

import com.gzdcdata.xxorder.entity.XxOrderDetail;
import com.gzdcdata.xxorderitem.entity.XxOrderItem;
import com.gzdcdata.xxproduct.entity.XxProduct;

public interface XxOrderService {
	/**
	 * 添加新订单
	 * @param addrId
	 * @param memo
	 * @param list
	 */
	String insertOrder(Long userId,Long addrId, String memo, List<XxProduct> list,String ipAddress) throws Exception;
	/**
	 * 分页查询列表订单信息 具体字段看实体类
	 * @param order_status
	 * @param userId
	 * @param page
	 * @param rows
	 * @return
	 */
	List<XxOrderDetail> listByStatusAndUser(Integer order_status, Long userId, Integer page, Integer rows) throws Exception;
	/**
	 * 统计指定状态订单数目
	 * @param memberId
	 * @param order_status
	 * @return
	 */
	Integer countByStatusMember(String memberId, String order_status);
	/**
	 * 取消订单
	 * @param userId
	 * @param orderId
	 */
	void cancelOrder(String userId, String sn) throws Exception;
	/**
	 * 获取订单详情
	 * @param sn
	 * @param memberId
	 * @return
	 */
	XxOrderDetail getOrderDetatil(String sn, String memberId);


	/**
	 * 确认收货
	 * @param userId
	 * @param sn
	 */
	void acceptOk(String userId, String sn);
	/**
	 * 通过订单编号查询商品快照
	 * @param sn
	 * @return
	 */
	List<XxOrderItem> getProductByOrderSn(String sn);

	/**
	 * 支付成功
	 * @param paymentId
	 * @param order_status
	 */
	Map<String, String> paySuccess(Map<String, String> map,int order_status) throws Exception;


	Map<String, String> payOrder(String paymentSn, String ipAddress,String code) throws Exception;

}
