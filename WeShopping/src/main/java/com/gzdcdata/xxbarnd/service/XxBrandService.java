package com.gzdcdata.xxbarnd.service;

import java.util.List;

import com.gzdcdata.xxbarnd.entity.XxBrand;

public interface XxBrandService {
	/**
	 * 获取所有品牌分类
	 * @return
	 * @throws Exception
	 */
	List<XxBrand> getList() throws Exception;

}
