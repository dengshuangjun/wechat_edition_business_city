package com.gzdcdata.xxbarnd.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxbarnd.dao.XxBrandDao;
import com.gzdcdata.xxbarnd.entity.XxBrand;
import com.gzdcdata.xxbarnd.service.XxBrandService;
/**
 * 品牌分类
 * @author Tianqing
 *
 *
 */
@Service("xxBrandService")
public class XxBrandServiceImp implements XxBrandService {
	@Resource(name="xxBrandDao")
	private XxBrandDao xxBrandDao;
	@Override
	public List<XxBrand> getList() throws Exception {
		return xxBrandDao.getList();
	}

}
