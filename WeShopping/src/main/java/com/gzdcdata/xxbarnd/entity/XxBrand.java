package com.gzdcdata.xxbarnd.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.ToString;
/**
 * 商品品牌分类实体
 * @author dsj
 *
 *
 */
@Data
@ToString
public class XxBrand {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private Integer orders;
	private String introduction;
	private String logo;
	private String name;
	private Integer type;
	private String url;

}
