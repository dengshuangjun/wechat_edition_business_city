package com.gzdcdata.xxbarnd.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.xxbarnd.entity.XxBrand;
import com.gzdcdata.xxbarnd.service.XxBrandService;

/**
 * 品牌分类控制器
 * @author dsj
 *
 *
 */
@Controller
@RequestMapping("xxBrand")
public class XxBrandController extends ApiController{

	@Resource(name="xxBrandService")
	private XxBrandService xxBrandService;

	/**
	 * 查询所有:以后如果多了可以考虑分页查询
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<ResponseData> list() {
		try {
			List<XxBrand> xxBrand=xxBrandService.getList();
			return new ResponseEntity<ResponseData>(success(xxBrand),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("未查询到相关数据"),HttpStatus.BAD_GATEWAY);
	}

}
