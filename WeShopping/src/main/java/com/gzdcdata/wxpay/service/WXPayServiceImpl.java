package com.gzdcdata.wxpay.service;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.wxpay.gzdcdata.pay.WXPay;
import com.wxpay.gzdcdata.pay.impl.WXPayConfigImpl;
/**
 * 微信支付实现类
 * @author dsj
 *
 *
 */
@Service("wXPayService")
public class WXPayServiceImpl implements WXPayService {
	private WXPay wxpay;
    private WXPayConfigImpl config;

	@Override
	public Map<String, String> doUnifiedOrder( HashMap<String, String> data) throws Exception{
		    config = WXPayConfigImpl.getInstance();
	        wxpay = new WXPay(config);
	        Map<String, String> r = wxpay.unifiedOrder(data);
	        return r;
	}
	@Override
	public Map<String, String> orderQuery(Map<String, String> reqData) throws Exception {
		config = WXPayConfigImpl.getInstance();
        wxpay = new WXPay(config);
        return wxpay.orderQuery(reqData);
	}



}
