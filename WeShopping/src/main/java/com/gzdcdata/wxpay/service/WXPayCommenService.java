package com.gzdcdata.wxpay.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gzdcdata.core.common.HttpClientUtil;
import com.gzdcdata.core.common.PublicInterfaceWX;
/**
 *	针对与收款公众号的一个服务层
 * @author dsj
 *
 *
 */
@Service("wXPayCommenService")
public class WXPayCommenService {

	/**
	 * 特殊情况:本商城登录授权的微信公众号是运行的公众号
	 * 在此支付的收款是另一个公众号服务,所有这个方法特别用来获取用户和收款公众号绑定的appid
	 * @return
	 */
	public JSONObject getWXInfo(String code) {
		try {
			String http = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
			http = http.replace("APPID", "wx3a59d3c5f923218e").replace("SECRET", "0733b1e17d45021c6769bba20c8b3dcc").replace("CODE", code);
			String msg = HttpClientUtil.httpGet(http);
			JSONObject json = JSON.parseObject(msg);
			return json;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static void main(String[] args) {
		System.out.println(new WXPayCommenService().getWXInfo("001Lp8XZ0mMl4Z1t6o00196uXZ0Lp8XX"));;
	}
}
