package com.gzdcdata.wxpay.service;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dsj
 *
 *
 */
public interface WXPayService {
	/**
	 * 统一下单
	 * @throws Exception
	 */
	 public Map<String, String> doUnifiedOrder( HashMap<String, String> data) throws Exception;

	 public Map<String, String> orderQuery(Map<String, String> reqData) throws Exception;

}
