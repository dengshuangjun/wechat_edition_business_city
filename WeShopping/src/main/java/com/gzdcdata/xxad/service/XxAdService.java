package com.gzdcdata.xxad.service;

import java.util.List;

import com.gzdcdata.xxad.entity.XxAd;

public interface XxAdService {
	/**
	 * 查询所有轮播图片信息
	 * @return
	 */
	List<XxAd> getAllAd();

}
