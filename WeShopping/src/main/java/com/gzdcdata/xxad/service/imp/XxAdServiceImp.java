package com.gzdcdata.xxad.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxad.dao.XxAddao;
import com.gzdcdata.xxad.entity.XxAd;
import com.gzdcdata.xxad.service.XxAdService;

/**
 * 首页轮播广告图片
 * @author dsj
 *
 *
 */
@Service("xxAdService")
public class XxAdServiceImp implements XxAdService{

	@Resource(name="xxAddao")
	private XxAddao xxAddao;

	@Override
	public List<XxAd> getAllAd() {
		return xxAddao.getAllAd();
	}

}
