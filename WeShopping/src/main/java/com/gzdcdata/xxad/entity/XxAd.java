package com.gzdcdata.xxad.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxAd {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;

	private Integer orders;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date begin_date;
	private String content;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date end_date;
	private String path;
	private String title;
	private Integer type;
	private String url;
	private Long ad_position;

}
