package com.gzdcdata.saxAddress.service.imp;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gzdcdata.core.common.Address;
import com.gzdcdata.saxAddress.dao.XxAreaDao;
import com.gzdcdata.saxAddress.service.SaxAddressService;
/**
 * 地址选择信息初始化服务层（远程请求）
 * @author dsj
 *
 */
@Service("saxAddressService")
public class SaxAddressServiceImp implements SaxAddressService {
		@Resource(name="xxAreaDao")
		private XxAreaDao xxAreaDao;
		private String RegionCountryUrl="http://ws.webxml.com.cn/WebServices/WeatherWS.asmx/getRegionCountry";
		// 获取省份的url
		private  String provUrl = "http://ws.webxml.com.cn/WebServices/WeatherWS.asmx/getRegionProvince";
		// 获取城市的url
		private  String cityUrl = "http://ws.webxml.com.cn/WebServices/WeatherWS.asmx/getSupportCityString?theRegionCode=";

		public List<Address> getAddress(String code) throws Exception{
			//Map<String,Object> map=new HashMap<String, Object>();
			List<Address> provList=new ArrayList<Address>();
			try {
				// 1、得到dom工厂解析实例
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				// 2、从dom解析工厂实例中得到dom解析器
				DocumentBuilder builder = factory.newDocumentBuilder();
				// 3、把要解释的xml放入解析器中
				InputStream is =null;
				if(code.equals("1")){//中国大陆
					is = getInputStream(provUrl);
				}else if(code.equals("0")){//海外其他
					is = getInputStream(RegionCountryUrl);
				}else{
					is = getInputStream(cityUrl+code);
				}
				Document doc =builder.parse(is);
				NodeList nl = doc.getElementsByTagName("string");
				Address address=null;
				for(int i=0,temp=nl.getLength();i<temp;i++){
					address=new Address();
					Node n = nl.item(i);
					String value=n.getFirstChild().getNodeValue().trim();
					address.setCode(value.substring(value.indexOf(",")+1));
					address.setName(value.substring(0,value.indexOf(",")));
					provList.add(address);
				}
				is.close();
				//map.put("status", 200);
				//map.put("data", provList);
			} catch (ParserConfigurationException e) {
				//map.put("status", 500);
				e.printStackTrace();
			} catch (SAXException e) {
				//map.put("status",500);
				e.printStackTrace();
			} catch (IOException e) {
				//map.put("status",500);
				e.printStackTrace();
			}
			return provList;
		}
		/**
		 * 接收网络请求，以流的形式返回
		 * @param url
		 * @return
		 */
		private  InputStream getInputStream(String url){
			InputStream is=null;
				try {
					//创建链接地址
					URL objUrl=new URL(url);
					//创建链接对象
					URLConnection urlCon = objUrl.openConnection();
					//设置请求属性
					urlCon.setRequestProperty("Host","ws.webxml.com.cn");
					//开始链接
					urlCon.connect();
					//接收返回数据
					is=urlCon.getInputStream();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			return is;
		}
		public List<Address> getLocalAddress(String code) throws Exception{
			return xxAreaDao.getLocalAddressByCode(code);
		}
}
