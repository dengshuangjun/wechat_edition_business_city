package com.gzdcdata.saxAddress.service;

import java.util.List;

import com.gzdcdata.core.common.Address;

public interface SaxAddressService {
	/**
	 * web服务获取： 该方法可能会获取不到
	 * 通过0海外大陆代码  1中国大陆代码获取省份信息，其他数字则获取城市地址信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	List<Address> getAddress(String code) throws Exception;
	/**
	 * 本地数据库获取地址信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Address> getLocalAddress(String code) throws Exception;
}
