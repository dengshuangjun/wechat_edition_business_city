package com.gzdcdata.saxAddress.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxArea {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private Long  orders;
	private String full_name;
	private String name;
	private String tree_path;
	private Long parent;
	/**
	 * 便于与网络端获取字段相同
	 * @return
	 */
	public Long getCode(){
		return id;
	}
}
