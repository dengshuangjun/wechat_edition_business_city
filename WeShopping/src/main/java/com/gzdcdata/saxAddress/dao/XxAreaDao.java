package com.gzdcdata.saxAddress.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gzdcdata.core.common.Address;

public interface XxAreaDao {

	List<Address> getLocalAddressByCode(@Param("code")String code);

}
