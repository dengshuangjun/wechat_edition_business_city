package com.gzdcdata.xxorderitem.service;

import java.util.List;

import com.gzdcdata.xxorderitem.entity.XxOrderItem;

public interface XxOrderItemService {
	/**
	 * 批量插入
	 * @param xxOrderItems
	 */
	void insertBatch(List<XxOrderItem> xxOrderItems) throws Exception;

}
