package com.gzdcdata.xxorderitem.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxorderitem.dao.XxOrderItemDao;
import com.gzdcdata.xxorderitem.entity.XxOrderItem;
import com.gzdcdata.xxorderitem.service.XxOrderItemService;
/**
 * 子订单信息
 * @author dsj
 *
 */
@Service("xxOrderItemService")
public class XxOrderItemServiceImp implements XxOrderItemService{

	@Resource(name="xxOrderItemDao")
	private XxOrderItemDao xxOrderItemDao;

	@Override
	public void insertBatch(List<XxOrderItem> xxOrderItems) throws Exception{
		if(xxOrderItems==null||xxOrderItems.size()<1){
			throw new Exception("批量插入的子订单数据不能为空");
		}
		xxOrderItemDao.insertBatch(xxOrderItems);
	}
}
