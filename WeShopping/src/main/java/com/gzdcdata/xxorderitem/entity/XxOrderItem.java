package com.gzdcdata.xxorderitem.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxOrderItem {
	private Long id;
	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private Date create_date;
	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private Date modify_date;
	private String full_name;
	private Boolean is_gift;
	private String name;
	private String price;
	private Integer quantity;
	private Integer return_quantity;
	private Integer shipped_quantity;
	private String sn;
	private String thumbnail;
	private Integer weight;
	private Long orders;
	private Long product;
}
