package com.gzdcdata.xxorderitem.dao;

import java.util.List;

import com.gzdcdata.xxorderitem.entity.XxOrderItem;

public interface XxOrderItemDao {
	/**
	 * 批量插入
	 * @param xxOrderItems
	 */
	void insertBatch(List<XxOrderItem> xxOrderItems);

	List<XxOrderItem> selListByOrderId(Long orderId);

	List<XxOrderItem> getByOrderSn(String sn);

}
