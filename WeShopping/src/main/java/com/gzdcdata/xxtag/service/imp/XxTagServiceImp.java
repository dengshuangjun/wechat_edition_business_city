package com.gzdcdata.xxtag.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxproduct.dao.XxProductDao;
import com.gzdcdata.xxproduct.entity.XxProduct;
import com.gzdcdata.xxtag.dao.XxTagDao;
import com.gzdcdata.xxtag.entity.XxTag;
import com.gzdcdata.xxtag.service.XxTagService;
/**
 * 标签逻辑层
 * @author dsj
 *
 *
 */
@Service("xxTagService")
public class XxTagServiceImp implements XxTagService {
	@Resource(name="xxTagDao")
	private XxTagDao xxTagDao;
	@Resource(name="xxProductDao")
	private XxProductDao  xxProductDao;

	@Override
	public List<XxTag> getAllTagsProduct() {
		List<XxTag> tagsProducts=xxTagDao.getAllTag();
		List<XxProduct> xxProducts = null;
		for(XxTag tagsProduct:tagsProducts){//只查询前三条
			xxProducts=getTagsProductBypage(tagsProduct.getId(),1,3,"modify_date desc").getList();
			tagsProduct.setXxProductList(xxProducts);
		}
		return tagsProducts;
	}
	@Override
	public PageInfo<XxProduct> getTagsProductBypage(Long tags,Integer page,Integer rows,String orderBy){
		PageHelper.startPage(page, rows);
		List<XxProduct> xxProducts=xxProductDao.findSameByTagId(tags,orderBy);
		PageInfo<XxProduct> pageInfo=new PageInfo<XxProduct>(xxProducts);
		return pageInfo;
	}

}
