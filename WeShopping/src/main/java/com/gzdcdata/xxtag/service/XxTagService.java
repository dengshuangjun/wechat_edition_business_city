package com.gzdcdata.xxtag.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxproduct.entity.XxProduct;
import com.gzdcdata.xxtag.entity.XxTag;

public interface XxTagService {
	/**
	 * 获取所有标签及标签商品信息
	 * @return
	 */
	List<XxTag> getAllTagsProduct();

	/**
	 * 根据标签分页查询商品
	 * @param tags
	 * @param page
	 * @param rows
	 * @param orderBy
	 * @return
	 */
	public PageInfo<XxProduct> getTagsProductBypage(Long tags,Integer page,Integer rows,String orderBy);
}
