package com.gzdcdata.xxtag.dao;

import java.util.List;

import com.gzdcdata.xxtag.entity.XxTag;

public interface XxTagDao {

	/**
	 * 查询所有商品标签
	 * @return
	 */
	List<XxTag> getAllTag();

}
