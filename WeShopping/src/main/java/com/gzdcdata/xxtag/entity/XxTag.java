package com.gzdcdata.xxtag.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gzdcdata.xxproduct.entity.XxProduct;

import lombok.Data;
/**
 * 首页标签实体类
 * @author dsj
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxTag {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private Integer orders;
	private String icon;
	private String memo;
	private String name;
	private Integer type;
	/**
	 * 所属标签的产品列表
	 */
	private List<XxProduct> xxProductList;
}
