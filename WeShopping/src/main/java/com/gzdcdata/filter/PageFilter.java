package com.gzdcdata.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.gzdcdata.core.common.ApiController;

/**
 * 浏览器过滤
 * @author dsj
 *
 */
public class PageFilter extends ApiController implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		logger.info("进入过滤器了。。。");
		HttpServletRequest req = ((HttpServletRequest)request);
		String user_agent = req.getHeader("user-agent");
		String url=req.getRequestURI();
		logger.info(url);
		///dcdata/text文章模块,允许任意访问
		if(url.indexOf("/dcdata/text")>=0||url.indexOf("/xxorder/paySuccess")>=0||url.indexOf("openError.html")>=0||user_agent.toLowerCase().indexOf("micromessenger")>=0){
			logger.info("通过页面过滤 ....");
			chain.doFilter(request, response);
		}else{
			req.getRequestDispatcher("/openError.html").forward(request, response);
		}
	}
	@Override
	public void destroy() {

	}
}
