package com.gzdcdata.home.web;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.PublicInterfaceWX;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.core.common.SHA1;

/**
 * 微信相关接口数据
 * @author dsj
 *
 *
 */
@Controller
@RequestMapping("/wxInterface")
public class WxInterfaceController extends ApiController{


	@GetMapping("/getSignature")
	public ResponseEntity<ResponseData> getSignature(String url){
		if(url==null||url.trim()==""){
			return new ResponseEntity<ResponseData>(fail("参数错误"),HttpStatus.BAD_REQUEST);
		}
		String access_Token=PublicInterfaceWX.getAccessToken();
		String ticket = PublicInterfaceWX.getTicketByAccessToken(access_Token);
		String noncestr =UUID.randomUUID().toString();
		long timestamp = System.currentTimeMillis();
		String signature = "jsapi_ticket="+ticket+"&noncestr="+noncestr+"&timestamp="+timestamp+"&url="+url+"";
		//byte[] signatures = DigestUtils.sha1(signature);
		SHA1 sha1 = new SHA1();
		String signatures = sha1.getDigestOfString(signature.getBytes());
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("timestamp", timestamp);
		map.put("noncestr", noncestr);
		map.put("signature", signatures);
		map.put("appid", PublicInterfaceWX.getAppid());
		return new ResponseEntity<ResponseData>(success(map),HttpStatus.OK);
    }


	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString());
	}
}
