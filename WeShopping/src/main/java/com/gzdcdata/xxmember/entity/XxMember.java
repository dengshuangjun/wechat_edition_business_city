package com.gzdcdata.xxmember.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class XxMember {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private String address;
	private String amount;
	//存储用户头像
	private String attribute_value0;
	private String attribute_value1;
	/*attribute_value1
	attribute_value2
	attribute_value3
	attribute_value4
	attribute_value5
	attribute_value6
	attribute_value7
	attribute_value8
	attribute_value9*/
	private String balance;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date birth;
	private String email;
	private Integer gender;
	private Boolean is_enabled;
	private Boolean is_locked;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date locked_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date login_date;
	private Integer login_failure_count;
	private String login_ip;
	private String mobile;
	private String name;
	private String password;
	private String phone;
	private Long point;
	private String register_ip;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date safe_key_expire;
	private String safe_key_value;
	private String username;
	private String zip_code;
	private Long area;
	private Long member_rank;
	private Long type;
	private Long member_type;
	private Long parent;
	private String rewards_money;
	private String nick_name;
	private String bonus;
	private String bonus_balance;
	private String bacode;
	private String club;
	private Boolean is_club_verify;
	private Boolean is_code_verify;
}
