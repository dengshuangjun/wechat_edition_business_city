package com.gzdcdata.xxmember.service;

import java.util.List;

import com.gzdcdata.xxmember.entity.XxMember;
import com.gzdcdata.xxproduct.entity.XxProduct;

public interface XxMemberService {
	/**
	 * 通过openid获取用户信息
	 * @param openid
	 * @return
	 */
	XxMember getXxMemberInfoByOpenid(String openid);
	/**
	 * 添加微信用户信息
	 * @param xxMemberTemp
	 */
	XxMember addUserInfo(XxMember xxMemberTemp);

	/**
	 * 更新用户收藏
	 * @param memberId
	 * @param productId
	 * @param option
	 */
	void updateFavorite(String memberId, String productId, String option) throws Exception;

	Boolean getShouCangByMidAndPid(String memberId, String productId);
	/**
	 * 分页查询会员收藏商品
	 * @param memberId
	 * @param page
	 * @param rows
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	List<XxProduct> selFavoriteByMember(String memberId, Integer page, Integer rows, String orderBy) throws Exception;

}
