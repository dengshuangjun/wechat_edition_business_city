package com.gzdcdata.xxmember.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxmember.dao.XxMemberDao;
import com.gzdcdata.xxmember.entity.XxMember;
import com.gzdcdata.xxmember.service.XxMemberService;
import com.gzdcdata.xxproduct.dao.XxProductDao;
import com.gzdcdata.xxproduct.entity.XxProduct;
/**
 * 会员逻辑层
 * @author dsj
 *
 */
@Service("xxMemberService")
public class XxMemberServiceImp implements XxMemberService {
	@Resource(name="xxMemberDao")
	private XxMemberDao xxMemberDao;
	@Resource(name="xxProductDao")
	private XxProductDao xxProductDao;
	@Override
	public XxMember getXxMemberInfoByOpenid(String openid) {
		return xxMemberDao.getXxMemberInfoByOpenid(openid);
	}
	@Override
	public XxMember addUserInfo(XxMember xxMemberTemp) {
		xxMemberTemp.setId(xxMemberDao.addUserInfo(xxMemberTemp));
		return xxMemberTemp;
	}
	@Override
	public void updateFavorite(String memberId, String productId, String option) throws Exception {
		if(option.equals("add")){
			xxMemberDao.addFavorite(memberId,productId);
		}else if(option.equals("cancel")){
			xxMemberDao.deleteFavorite(memberId,productId);
		}else{
			throw new Exception("参数错误");
		}

	}
	@Override
	public Boolean getShouCangByMidAndPid(String memberId, String productId) {
		int result=xxMemberDao.countShouCangByMidAndPid(memberId,productId);
		if(result>0)
			return true;
		return false;
	}
	@Override
	public List<XxProduct> selFavoriteByMember(String memberId, Integer page, Integer rows, String orderBy) throws Exception{
		PageHelper.startPage(page, rows);
		List<XxProduct> list=xxProductDao.selFavoriteProductByMember(memberId,orderBy);
		PageInfo<XxProduct> pageInfo=new PageInfo<XxProduct>(list);
		return pageInfo.getList();
	}

}
