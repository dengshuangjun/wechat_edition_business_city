package com.gzdcdata.xxmember.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gzdcdata.core.common.Address;
import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.core.common.StringUtil;
import com.gzdcdata.saxAddress.service.SaxAddressService;
import com.gzdcdata.xxmember.entity.XxMember;
import com.gzdcdata.xxmember.service.XxMemberService;
import com.gzdcdata.xxproduct.entity.XxProduct;
import com.gzdcdata.xxreceiver.entity.XxReceiver;
import com.gzdcdata.xxreceiver.service.XxReceiverService;

/**
 * 用户相关操作控制类
 * @author dsj
 *
 *
 */
@Controller
@RequestMapping("/xxmember")
public class XxMemberController extends ApiController{
	@Resource(name="xxReceiverService")
	private XxReceiverService xxReceiverService;
	@Resource(name="saxAddressService")
	private SaxAddressService saxAddressService;
	@Resource(name="xxMemberService")
	private XxMemberService xxMemberService;
	/**
	 * 通过用户id获取地址信息，同时如果传入选中的地址id则将该id的地址设置为默认
	 * @param id
	 * @param addrId
	 * @return
	 */
	@GetMapping("/userAddrList")
	public ResponseEntity<ResponseData> userAddrList(String id,String addrId){
		logger.info("根据用户id请求地址信息");
		try {
			List<XxReceiver>  addrList=xxReceiverService.getUserAddrListByMember(id,addrId);
			return new ResponseEntity<ResponseData>(success(addrList), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseData>(fail("获取失败!"), HttpStatus.BAD_GATEWAY);
		}
	}
	/**
	 * 通过code获取相关地址选择列表
	 * @param code
	 * @return
	 */
	@GetMapping("/addressSelByCode")
	public ResponseEntity<ResponseData> getAddressSelByCode(String code){
		logger.info("通过code获取相关地址选择列表");
		try {
			List<Address> result = saxAddressService.getLocalAddress(code);
			return new ResponseEntity<ResponseData>(success(result), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseData>(fail("获取失败!"), HttpStatus.BAD_GATEWAY);
		}
	}
	/**
	 * 更新用户对商品收藏状态
	 * @param memberId 会员id
	 * @param productId 产品id
	 * @param option 操作类型:两个值,一个add 一个为cancel
	 * @return
	 */
	@PostMapping("/updateFavorite")
	public ResponseEntity<ResponseData> updateFavorite(String memberId,String productId,
			@RequestParam(name="option",defaultValue="add")String option){
		logger.info("更新用户对商品收藏状态");
		if(memberId==null||!StringUtil.isNumeric(memberId)||productId==null||productId.trim().equals("")){
			return new ResponseEntity<ResponseData>(fail("请求错误!"), HttpStatus.BAD_GATEWAY);
		}
		if(option.trim().equals("")||(!option.equals("add")&&!option.equals("cancel"))){
			option="add";
		}
		try {
			xxMemberService.updateFavorite(memberId,productId,option);
			return new ResponseEntity<ResponseData>(success("操作成功!"), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseData>(fail("处理异常!"), HttpStatus.BAD_GATEWAY);
		}
	}
	/**
	 * 获取商品收藏状态
	 * @param memberId
	 * @param productId
	 * @return
	 */
	@GetMapping("/getShouCangByMidAndPid")
	public ResponseEntity<ResponseData> getShouCangByMidAndPid(String memberId,String productId){
		if(memberId==null||!StringUtil.isNumeric(memberId)||productId==null||productId.trim().equals("")){
			return new ResponseEntity<ResponseData>(fail("请求错误!"), HttpStatus.BAD_GATEWAY);
		}
		return new ResponseEntity<ResponseData>(success(xxMemberService.getShouCangByMidAndPid(memberId,productId)), HttpStatus.OK);
	}
	/**
	 * 分页获取用户的收藏商品
	 * @param memberId
	 * @param page
	 * @param rows
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/selFavoriteByMember")
	public ResponseEntity<ResponseData> selFavoriteByMember(
			@RequestParam(name="memberId") String memberId,
			@RequestParam(name="page",defaultValue="1")Integer page,
			@RequestParam(name="rows",defaultValue="10") Integer rows,
			@RequestParam(name="orderBy",defaultValue="create_date") String orderBy){
		logger.info("分页获取用户的收藏商品");
		if(memberId==null||memberId.trim()==""){
			return new ResponseEntity<ResponseData>(fail("请求参数错误!"), HttpStatus.BAD_GATEWAY);
		}
		try {
			List<XxProduct> list = xxMemberService.selFavoriteByMember(memberId,page,rows,orderBy);
			return new ResponseEntity<ResponseData>(success(list), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("获取数据错误!"), HttpStatus.BAD_GATEWAY);

	}

}