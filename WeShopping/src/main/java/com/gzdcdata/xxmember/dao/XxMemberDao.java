package com.gzdcdata.xxmember.dao;

import com.gzdcdata.xxmember.entity.XxMember;

public interface XxMemberDao {

	XxMember getXxMemberInfoByOpenid(String openid);

	Long addUserInfo(XxMember xxMemberTemp);

	void addFavorite(String memberId, String productId);

	void deleteFavorite(String memberId, String productId);

	int countShouCangByMidAndPid(String memberId, String productId);

}
