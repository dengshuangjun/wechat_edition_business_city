package com.gzdcdata.core.common;
/**
 * 字符串工具类
 * @author dsj
 *
 *
 */
public class StringUtil {
	private static String splits=",";

	public final static boolean isNumeric(String s) {
	    if (s != null && !"".equals(s.trim()))
	        return s.matches("^[0-9]*$");
	    else
	        return false;
	}
	/**
	 * 把str通过split字符截取成数组并返回 清空了为空的情况
	 * @param str
	 * @param split
	 * @return
	 */
	public final static String[] strSplitToArray(String str,String split){
		if(str==null||str==""){
			return null;
		}
		if(splits!=null&&split.trim()!=""){
			StringUtil.splits=split;
		}else{
			return null;
		}
		if(str.startsWith(splits)){
			str=str.substring(str.indexOf(splits)+splits.length());
		}
		if(str.endsWith(splits)){
			str=str.substring(0,str.lastIndexOf(splits));
		}
		return str.split(splits);
	}

	public final static boolean isNotEmpty(String str){
		if(str==null||"".equals(str)){
			return false;
		}
		return true;
	}
}
