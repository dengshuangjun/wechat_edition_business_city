package com.gzdcdata.core.common;

import lombok.Data;
import lombok.ToString;

/**
 * 地址信息实体
 * @author dsj
 *
 */
@Data
@ToString
public class Address {
		private String code;
		private String name;
}
