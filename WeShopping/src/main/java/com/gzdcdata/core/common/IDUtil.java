package com.gzdcdata.core.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
/**
 * 编号生成
 * @author dsj
 *
 *
 */
public class IDUtil {

	/**
	 * 图片名生成
	 */
	public static String genImageName() {
		//取当前时间的长整形值包含毫秒
		long millis = System.currentTimeMillis();
		//long millis = System.nanoTime();
		//加上三位随机数
		Random random = new Random();
		int end3 = random.nextInt(999);
		//如果不足三位前面补0
		String str = millis + String.format("%03d", end3);

		return str;
	}

	/**
	 * 商品id生成
	 */
	public static long genItemId() {
		//取当前时间的长整形值包含毫秒
		long millis = System.currentTimeMillis();
		//long millis = System.nanoTime();
		//加上两位随机数
		Random random = new Random();
		int end2 = random.nextInt(99);
		//如果不足两位前面补0
		String str = millis + String.format("%02d", end2);
		long id = new Long(str);
		return id;
	}

	/**
	 * 订单编号生成
	 * @return
	 */
	public static long orderId(){
		SimpleDateFormat form = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date=new Date();
		String str=form.format(date);
		//加上三位随机数
		Random random = new Random();
		int end3 = random.nextInt(9999);
		//如果不足三位前面补0
		str = str + String.format("%04d", end3);
		return new Long(str);
	}
}
