package com.gzdcdata.core.common;

import java.io.IOException;
import java.util.List;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 初步版本，不考虑接口资源限制
 *
 * @author chenligang@szslcpay.com
 *
 */
@SuppressWarnings("unchecked")
public class PublicInterfaceWX {
	static String APPID = "wx964a7f6d96826955";
	static String SECRET = "cdb732005f7289a775e1c8a679682f41";


	public static String  getAppid(){
		return APPID;
	}
	/**
	 *
	 *
	 * 返回 AccessToken
	 *
	 * @return
	 * @throws IOException
	 */
	public static String getAccessToken() {
		try {
			String access_token = "";
			String http = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
			http = http.replace("APPID", APPID).replace("APPSECRET", SECRET);
			String msg = HttpClientUtil.httpGet(http);
			JSONObject json = JSON.parseObject(msg);
			if (json != null) {
				access_token = json.getString("access_token");
			}
			return access_token;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * 不需要弹框授权
	 * 根据CODE获取 返回openId
	 *
	 * @return
	 * @throws IOException
	 */
	public static String getOpenid(String code) {
		try {
			String openid = "";
			String http = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
			http = http.replace("APPID", APPID).replace("SECRET", SECRET).replace("CODE", code);
			String msg = HttpClientUtil.httpGet(http);
			JSONObject json = JSON.parseObject(msg);
			if (json.getString("openid") != "") {
				openid = json.getString("openid");
			}
			return openid;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 *
	 * 根据CODE获取微信用户信息
	 * 必须 scope=snsapi_userinfo 时才可以用
	 * 获取微信用户相关信息（包含OPENID）
	 * @return
	 * @throws IOException
	 */
	public static JSONObject getWXInfo(String code) {
		try {
			String http = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
			http = http.replace("APPID", APPID).replace("SECRET", SECRET).replace("CODE", code);
			String msg = HttpClientUtil.httpGet(http);
			JSONObject json = JSON.parseObject(msg);
			if (json != null) {
				String access_token = json.getString("access_token");
				String openid = json.getString("openid");
				json = getWXInfo(openid, access_token);
			}
			return json;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * 根据openid和token获取微信信息
	 *
	 * @param openid
	 * @param access_token
	 * @return
	 */
	public static JSONObject getWXInfo(String openid, String access_token) {
		try {
			if (access_token != null && openid != null) {
				String http = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
				http = http.replace("ACCESS_TOKEN", access_token).replace("OPENID", openid);
				String msg = HttpClientUtil.httpGet(http);
				JSONObject json = JSON.parseObject(msg);
				return json;
			}
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	/**
	 *  权限申请通知  群发
	 * h5T0U4hmgxCGkTpHqjqrzfN6nR4P_kaD-4ROXnmK1kg
	 *  1=OPENID
	 * @param list
	 * @return
	 */
	public static String  templateSendTicketList(List<String> list){
		 String[]  openids = list.get(0).split(",");
		for (int i = 0; i < openids.length; i++) {
			list.remove(0);
			list.add(0,openids[i].trim());
			String msg = templateSendTicket(list);
			System.out.println("发送模板消息="+msg);
		}
       return "发送了"+openids.length+"条消息";
	}
	/**
	 *  权限申请通知
	 * h5T0U4hmgxCGkTpHqjqrzfN6nR4P_kaD-4ROXnmK1kg
	 *  1=OPENID
	 * @param list
	 * @return
	 */
	public static String  templateSendTicket(List<String> list){
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
		String access_token = getAccessToken();
		url = url.replace("ACCESS_TOKEN", access_token);
    	String templateId ="h5T0U4hmgxCGkTpHqjqrzfN6nR4P_kaD-4ROXnmK1kg";
	    String jsonText="{\"touser\":\"OPENID\",\"url\":\"URL\" ,\"template_id\":\"templateId\","
	    		+ "\"topcolor\":\"#FF0000\",\"data\":{\"first\": {\"value\":\"firstData\",\"color\":\"#173177\"},"
	    		+ "\"keyword1\": {\"value\":\"keyword1Data\",\"color\":\"#173177\"},"
	    		+ "\"keyword2\": {\"value\":\"keyword2Data\",\"color\":\"#173177\"},"
	    		+ "\"keyword3\": {\"value\":\"keyword3Data\",\"color\":\"#173177\"},"
	    		+ "\"keyword4\": {\"value\":\"keyword4Data\",\"color\":\"#173177\"},"
	    		+ "\"keyword5\": {\"value\":\"keyword5Data\",\"color\":\"#173177\"},"
	    		+ "\"remark\": {\"value\":\"remarkData\",\"color\":\"#FF0000\"}}}";
		jsonText = jsonText.replace("OPENID", list.get(0)).replace("templateId", templateId).
				replace("URL", list.get(8)).
				replace("firstData", list.get(1)).replace("keyword1Data", list.get(2)).
				replace("keyword2Data", list.get(3)).replace("keyword3Data", list.get(4)).
				replace("keyword4Data", list.get(5)).replace("keyword5Data", list.get(6)).
				replace("remarkData", list.get(7));
		System.out.println(jsonText);
        String msg = HttpClientUtil.doPost(url, jsonText , "UTF-8","application/json");
        return msg;
	}
	/**
	 *  审核通过通知
	 * f7VF5yqRlOqLnm0xez6QrwNVYsUF0PIhHx5e6RzxVpQ
	 *  1=OPENID
	 * @param list
	 * @return
	 */
	public static String  templateSendPassed(List<String> list){
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
		String access_token = getAccessToken();
		url = url.replace("ACCESS_TOKEN", access_token);
		String templateId ="f7VF5yqRlOqLnm0xez6QrwNVYsUF0PIhHx5e6RzxVpQ";
		String jsonText="{\"touser\":\"OPENID\",\"url\":\"URL\" ,\"template_id\":\"templateId\","
				+ "\"topcolor\":\"#FF0000\",\"data\":{\"first\": {\"value\":\"firstData\",\"color\":\"#173177\"},"
				+ "\"keyword1\": {\"value\":\"keyword1Data\",\"color\":\"#173177\"},"
				+ "\"keyword2\": {\"value\":\"keyword2Data\",\"color\":\"#173177\"},"
				+ "\"keyword3\": {\"value\":\"keyword3Data\",\"color\":\"#173177\"},"
				+ "\"remark\": {\"value\":\"remarkData\",\"color\":\"#FF0000\"}}}";
		jsonText = jsonText.replace("OPENID", list.get(0)).replace("templateId", templateId).
				replace("URL", list.get(6)).
				replace("firstData", list.get(1)).replace("keyword1Data", list.get(2)).
				replace("keyword2Data", list.get(3)).replace("keyword3Data", list.get(4)).
				replace("remarkData", list.get(5));
		System.out.println(jsonText);
		String msg = HttpClientUtil.doPost(url, jsonText , "UTF-8","application/json");
		return msg;
	}
	/**
	 * 通过access_token 获取tiket用于本地加密签名
	 * @param access_token
	 * @return
	 */
	public static String getTicketByAccessToken(String access_token){
		try {
			String ticket = "";
			String http = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
			http = http.replace("ACCESS_TOKEN", access_token);
			String msg = HttpClientUtil.httpGet(http);
			JSONObject json = JSON.parseObject(msg);
			if (json != null) {
				ticket = json.getString("ticket");
			}
			return ticket;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 *
	 * @param list
	 *  0：接收人OPENID
	 *  1 ： 标题
	 *  2 : 收件人
	 *  3： 手机号
	 *  4：详细地址
	 *  5：说明（可以是订单信息 金额信息）
	 * @return  发送模板结果
	 */
	public static String goodsDeliver(List<String> list) {
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
		String access_token = getAccessToken();
		url = url.replace("ACCESS_TOKEN", access_token);
		String templateId ="EJGEPNfLSVLmmJF9YnFgzKQPBYFFDYBjXIswQq2mlt8";
		JSONObject json = new JSONObject();
		json.put("touser", list.get(0));
		json.put("template_id", templateId);
		json.put("topcolor", "#FF0000");
		JSONObject data = new JSONObject();

		JSONObject jsonFirst = new JSONObject();
        jsonFirst.put("value", list.get(1));
        jsonFirst.put("color", "#173177");
		data.put("first",JSONObject.parseObject("{\"value\":\""+list.get(1)+"\",\"color\":\"#9a9a9a\"}"));
		data.put("keyword1", JSONObject.parseObject("{\"value\":\""+list.get(2)+"\",\"color\":\"#173177\"}"));
		data.put("keyword2", JSONObject.parseObject(" {\"value\":\""+list.get(3)+"\",\"color\":\"#173177\"}"));
		data.put("keyword3", JSONObject.parseObject(" {\"value\":\""+list.get(4)+"\",\"color\":\"#173177\"}"));
		data.put("remark", JSONObject.parseObject(" {\"value\":\""+list.get(5)+"\",\"color\":\"#9a9a9a\"}"));
		json.put("data", data);
		String msg =  HttpClientUtil.doPost(url, json.toString() , "UTF-8","application/json");
		return msg;
	}


}
