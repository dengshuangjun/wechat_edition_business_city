package com.gzdcdata.xxreceiver.web;

import java.util.List;

import javax.annotation.Resource;
/**
 * 收货地址控制器
 */

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 客户地址信息控制器
 * @author dsj
 *
 */

import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.xxreceiver.entity.XxReceiver;
import com.gzdcdata.xxreceiver.service.XxReceiverService;
@Controller
@RequestMapping("/xxreceiver")
public class XxReceiverController extends ApiController{

	@Resource(name="xxReceiverService")
	private XxReceiverService xxReceiverService;

	/**
	 * 添加地址信息
	 * @param xxReceiver
	 * @return
	 */
	@PostMapping("/addReceiver")
	public ResponseEntity<ResponseData> addReceiver(XxReceiver xxReceiver){
		try {
			xxReceiverService.addReceiver(xxReceiver);
			return new ResponseEntity<ResponseData>(success(),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("添加失败！"),HttpStatus.BAD_REQUEST);
	}
	/**
	 * 查询某个用户的所有地址信息
	 * @param memberId
	 * @return
	 */
	@GetMapping("/listByMember")
	public ResponseEntity<ResponseData> getUserAddrListByMember(String memberId){
		try {
			List<XxReceiver> result = xxReceiverService.getUserAddrListByMember(memberId, null);
			return new ResponseEntity<ResponseData>(success(result),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("查询失败失败！"),HttpStatus.BAD_REQUEST);
	}
	/**
	 * 删除地址信息通过id
	 * @param id
	 * @return
	 */
	@GetMapping("del")
	public ResponseEntity<ResponseData> del(String id){
		int result=xxReceiverService.delById(id);
		if(result>0){
			return new ResponseEntity<ResponseData>(success("删除成功!"),HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData>(fail("删除失败！"),HttpStatus.BAD_REQUEST);
	}

}
