package com.gzdcdata.xxreceiver.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gzdcdata.xxreceiver.entity.XxReceiver;

public interface XxReceiverDao {
	/**
	 * 通过客户id获取所有地址信息
	 * @param memberId
	 * @return
	 */
	List<XxReceiver> getXxReceiverByMemberId(@Param("memberId") String memberId);
	/**
	 * 更新指定用户id指定地址id的状态
	 * @param addrId地址id
	 * @param memberId 用户id
	 * @param is_default 状态：0和1
	 * @return
	 */
	int updateIsDefault(@Param("memberId") String memberId, @Param("addrId")String addrId,@Param("is_default") Integer is_default);
	/**
	 * 添加
	 * @param xxReceiver
	 */
	void insert(XxReceiver xxReceiver);
	/**
	 * 通过id查询地址信息
	 * @param addrId
	 * @return
	 */
	XxReceiver getById(@Param("id")Long id);

	int delById(@Param("id")String id);
}
