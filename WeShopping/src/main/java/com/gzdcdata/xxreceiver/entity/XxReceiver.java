package com.gzdcdata.xxreceiver.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;
/**
 * 收获地址实体
 * @author dsj
 *
 *
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxReceiver {
	private Long id;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT-8")
	private Date create_date;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT-8")
	private Date modify_date;
	private String address;
	private String area_name;
	private String consignee;
	private Boolean is_default;
	private String phone;
	private String zip_code;
	private Long area;
	private Long member;

}
