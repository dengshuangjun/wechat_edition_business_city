package com.gzdcdata.xxreceiver.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxreceiver.dao.XxReceiverDao;
import com.gzdcdata.xxreceiver.entity.XxReceiver;
import com.gzdcdata.xxreceiver.service.XxReceiverService;
/**
 * 客户地址地址服务
 * @author dsj
 *
 *
 */
@Service("xxReceiverService")
public class XxReceiverServiceImp implements XxReceiverService{

	@Resource(name="xxReceiverDao")
	private XxReceiverDao xxReceiverDao;

	@Override
	public List<XxReceiver> getUserAddrListByMember(String id,String addrId) throws Exception {
		if(id==null||id==""){
			throw new Exception("参数id不能为空");
		}
		if(addrId!=null){
			xxReceiverDao.updateIsDefault(id,null,0);//将所有地址状态更新为0
			xxReceiverDao.updateIsDefault(id,addrId,1);//更新参数id的地址信息为默认选中
		}
		return xxReceiverDao.getXxReceiverByMemberId(id);
	}
	//member:16689
		//area:833
		//phone:15616644140
		//consignee:zhangsna
		//area_name:河北安新
		//address:decheng shuju
	@Override
	public void addReceiver(XxReceiver xxReceiver) throws Exception {
		if(xxReceiver.getMember()==null){
				throw new Exception();
		}
		xxReceiver.setZip_code("000000");
		xxReceiver.setIs_default(false);
		xxReceiverDao.insert(xxReceiver);
	}
	@Override
	public int delById(String id) {
		return xxReceiverDao.delById(id);
	}
}
