package com.gzdcdata.xxreceiver.service;

import java.util.List;

import com.gzdcdata.xxreceiver.entity.XxReceiver;

public interface XxReceiverService {
	/**
	 * 通过用户id获取地址信息，同时如果传入地址id则将该id的地址设置为默认
	 * @param id 用户id
	 * @param addrId 地址id
	 * @return
	 * @throws Exception
	 */
 	List<XxReceiver> getUserAddrListByMember(String id, String addrId) throws Exception;
	/**
	 * 添加地址信息 其中的member值要有
	 * @param xxReceiver
	 * @throws Exception
	 */
	void addReceiver(XxReceiver xxReceiver)throws Exception;
	/**
	 * 删除地址
	 * @param id
	 * @return
	 */
	int delById(String id);

}
