package com.gzdcdata.xxproductproductimage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
/**
 * 产品图片
 * @author dsj
 *
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxProductProductImage {
	//产品id
	private Long product;
	//原图
	private String large;
	//略缩图
	private String medium;
	private Integer orders;
	//跟原图一样大
	private String source;
	//icon图片大小
	private String thumbnail;
	//图片名字
	private String title;
}
