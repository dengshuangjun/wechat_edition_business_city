package com.gzdcdata.xxproduct.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gzdcdata.xxproductproductimage.entity.XxProductProductImage;

import lombok.Data;
/**
 *产品实体类
 * @author dsj
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxProduct {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private Integer allocated_stock;
	private Double cost;
	private String full_name;
	private Long hits;
	private String image;
	private String introduction;
	private Boolean is_gift;
	private Boolean is_list;
	private Boolean is_marketable;
	private Boolean is_top;
	private String keyword;
	private String market_price;
	private String memo;
	private Long month_hits;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date month_hits_date;
	private Long month_sales;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date month_sales_date;
	private String name;
	private Long point;
	private String price;
	private Long sales;
	private Float score;
	private Long score_count;
	private String seo_description;
	private String seo_keywords;
	private String seo_title;
	private String sn;
	private Integer stock;
	private String stock_memo;
	private Long total_score;
	private String unit;
	private Long week_hits;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date week_hits_date;
	private Long week_sales;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date week_sales_date;
	private Integer weight;
	private Long brand;
	private Long goods;
	private Long product_category;
	private Long points_deduction;
	private Boolean is_free_shipping;
	private Boolean is_search;
	private Integer orders;
	private Long factoryId;
	private Long merch_product ;
	private Boolean is_service_bonus ;
	/*attribute_value0;
	attribute_value1;
	attribute_value10;
	attribute_value11;
	attribute_value12;
	attribute_value13;
	attribute_value14;
	attribute_value15;
	attribute_value16;
	attribute_value17;
	attribute_value18;
	attribute_value19;
	attribute_value2;
	attribute_value3;
	attribute_value4;
	attribute_value5;
	attribute_value6;
	attribute_value7;
	attribute_value8;
	attribute_value9;*/
	//非数据库字段
	private List<XxProductProductImage> xxProductProductImagelist;

	private Integer quantity;
	private String thumbnail;
	//评论数目
	private String reviews;
}
