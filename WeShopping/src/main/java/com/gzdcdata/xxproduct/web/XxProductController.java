package com.gzdcdata.xxproduct.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
/**
 * 产品信息控制器
 * @author dsj
 *
 *
 */
import com.github.pagehelper.PageInfo;
import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.PublicInterfaceWX;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.xxorder.dao.XxOrderDao;
import com.gzdcdata.xxorder.entity.XxOrder;
import com.gzdcdata.xxpayment.service.XxPaymentService;
import com.gzdcdata.xxproduct.entity.XxProduct;
import com.gzdcdata.xxproduct.service.XxProductService;
import com.gzdcdata.xxreview.entity.XxReview;
import com.gzdcdata.xxreview.service.XxReviewService;
import com.gzdcdata.xxtag.service.XxTagService;
@Controller
@RequestMapping("/product")
public class XxProductController extends ApiController{
	@Resource(name="xxProductService")
	private XxProductService xxProductService;
	@Resource(name="xxTagService")
	private XxTagService xxTagService;
	@Resource(name="xxReviewService")
	private XxReviewService xxReviewService;
	/**
	 * 分页获取产品信息
	 * @param page
	 * @param rows
	 * @return
	 */
	@GetMapping("/listPage")
	public ResponseEntity<ResponseData> listProductByPage(Integer page,Integer rows){
		logger.info("分页数据获取：页数"+page+"每页显示数："+rows);
		if(page==null||rows==null){
			return new ResponseEntity<ResponseData>(fail("参数不正确！"),HttpStatus.BAD_GATEWAY);
		}
		PageInfo<XxProduct> pageObject=xxProductService.getXxProductByPage(page, rows);
		return new ResponseEntity<ResponseData>(success(pageObject),HttpStatus.OK);
	}
	/**
	 * 通过商品获取商品详情信息
	 * @param id
	 * @return
	 */
	@GetMapping("/productItemById")
	public ResponseEntity<ResponseData> productItemById(Long id){
		logger.info("通过id获取商品信息 id"+id);
		if(id==null){
			return new ResponseEntity<ResponseData>(fail("参数不正确！"),HttpStatus.BAD_GATEWAY);
		}
		XxProduct xxProduct=xxProductService.getXxProductByid(id);
		return new ResponseEntity<ResponseData>(success(xxProduct),HttpStatus.OK);
	}
	/**
	 * 通过产品分类或者品牌分页并按照某个规则排序查询产品列表
	 * @param product_category 分类id
	 * @param page  页数
	 * @param rows  行数
	 * @param orderBy  排序规则
	 * @return
	 */
	@RequestMapping("/listOrderBy")
	public ResponseEntity<ResponseData> listOrderBy(
			String brandId,
			String product_category,
			@RequestParam(name="page",defaultValue="1")  Integer page,
			@RequestParam(name="rows",defaultValue="10")  Integer rows,
			@RequestParam(name="orderBy",defaultValue="modify_date desc")  String orderBy){
		logger.info("条件查询产品  产品品牌id为"+brandId);
		PageInfo<XxProduct> pageObject;
		try {
			pageObject = xxProductService.listOrderBy(brandId,product_category,page, rows,orderBy);
			return new ResponseEntity<ResponseData>(success(pageObject),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("请求失败！"),HttpStatus.BAD_GATEWAY);
	}
	/**
	 * 根据标签分页查询商品并排序
	 * @param tags
	 * @param page
	 * @param rows
	 * @param orderBy
	 * @return
	 */
	@RequestMapping("/listPageByTagsAndOrderBy")
	public ResponseEntity<ResponseData> listPageByTagsAndOrderBy(
			Long tags,
			@RequestParam(name="page",defaultValue="1")  Integer page,
			@RequestParam(name="rows",defaultValue="10")  Integer rows,
			@RequestParam(name="orderBy",defaultValue="modify_date desc")String orderBy){
		try {
			PageInfo<XxProduct> pageObject = xxTagService.getTagsProductBypage(tags, page, rows, orderBy);
			return new ResponseEntity<ResponseData>(success(pageObject),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("请求失败！"),HttpStatus.BAD_GATEWAY);
	}
	/**
	 * 分页获取产品评论
	 * @param productId
	 * @return
	 */
	@RequestMapping("/getreviewByProduct")
	public ResponseEntity<ResponseData> getreviewByProduct(String productId,
			@RequestParam(name="page",defaultValue="1")  Integer page,
			@RequestParam(name="rows",defaultValue="10")  Integer rows){
		try {
			List<XxReview> list=xxReviewService.selectreviewByProductId(productId,page,rows);
			return new ResponseEntity<ResponseData>(success(list),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseData>(fail("请求失败！"),HttpStatus.BAD_GATEWAY);
		}

	}

	@Resource(name="xxPaymentService")
	private XxPaymentService xxPaymentService;
	@Resource(name="xxOrderDao")
	private XxOrderDao xxOrderDao;
	/**
	 *
	 * @return
	 */
	/*@RequestMapping("/getopenIdByOrderId")
	public ResponseEntity<ResponseData> getopenIdByOrderId(){
		logger.info("测试接口--------");
		List<Map<String, Object>>  list1 = xxProductService.getopenIdByOrderId(Long.valueOf("49907"));
		xxPaymentService.updateStatusByPaymentId(String.valueOf(37522), 1); //收款单已支付
    	//通知卖家发货
    	// 可能存在一个订单购买多个商家商品的情况
    	XxOrder xxorder = xxOrderDao.selectOrderById(Long.valueOf( 49910 ));
    	System.out.println("============="+JSONObject.toJSONString(xxorder));

    	List<Map<String, Object>>  openids = xxProductService.getopenIdByOrderId(xxorder.getId());

        for (Map<String, Object> openid : openids) {
            List<String> list = new ArrayList<String>();
        	list.add((String)openid.get("openid"));
        	list.add("您好，您的客户已付款，请尽快发货哦");
        	list.add(xxorder.getConsignee());
        	list.add(xxorder.getPhone());
        	list.add(xxorder.getArea_name()+" "+xxorder.getAddress());
        	list.add("请登录商城管理平台确认发货");
        	String msg = PublicInterfaceWX.goodsDeliver(list);
        	System.out.println("模板发送结果==="+msg);
		}
		return new ResponseEntity<ResponseData>(success(list1),HttpStatus.OK);
	}*/
}
