package com.gzdcdata.xxproduct.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxproduct.entity.XxProduct;

public interface XxProductService {
	/**
	 * 分页查询每日推荐商品
	 * @param page
	 * @param rows
	 * @return
	 */
	PageInfo<XxProduct> getXxProductByPage(int page, int rows);
	/**
	 * 前台获取商品展示信息
	 * @param id
	 * @return
	 */
	XxProduct getXxProductByid(Long id);
	/**
	 * 分页获取指定分类的商品并排序
	 * @param product_category
	 * @param page
	 * @param rows
	 * @param orderBy
	 * @return
	 */
	PageInfo<XxProduct> listOrderBy(String brandId,String product_category,
			Integer page, Integer rows, String orderBy)  throws Exception;

	/**
	 * 根据订单ID
	 * @param id
	 * @return   订单商品的所属分类 的一级分类 openid
	 *           xx_product_category.openid
	 */
	List<Map<String, Object>> getopenIdByOrderId(Long orderId);

}
