package com.gzdcdata.xxproduct.service.imp;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxproduct.dao.XxProductDao;
import com.gzdcdata.xxproduct.entity.XxProduct;
import com.gzdcdata.xxproduct.service.XxProductService;
/**
 * 商品逻辑层
 * @author dsj
 *
 */
@Service("xxProductService")
public class XxProductServiceImp implements XxProductService{
	@Resource(name="xxProductDao")
	private XxProductDao xxProductDao;
	@Override
	public PageInfo<XxProduct> getXxProductByPage(int page, int rows) {
		//开始分页
		PageHelper.startPage(page, rows);
		List<XxProduct> list=xxProductDao.getXxProductByPage();
		//获取分页对象
		PageInfo<XxProduct> pageInfo=new PageInfo<XxProduct>(list);
		return pageInfo;
	}
	@Override
	public XxProduct getXxProductByid(Long id) {
		XxProduct xxProduct=xxProductDao.getXxProductByid(id);
		if(xxProduct!=null&&xxProduct.getIntroduction()!=null){
			xxProduct.setIntroduction(xxProduct.getIntroduction().replaceAll("class", ""));
		}
		return xxProduct;
	}
	@Override
	public PageInfo<XxProduct> listOrderBy(String brandId,String product_category, Integer page, Integer rows, String orderBy)  throws Exception{
		//开始分页
		PageHelper.startPage(page, rows);
		List<XxProduct> list=xxProductDao.listOrderBy(brandId,product_category,orderBy);
		//获取分页对象
		PageInfo<XxProduct> pageInfo=new PageInfo<XxProduct>(list);
		return pageInfo;
	}
	@Override
	public  List<Map<String, Object>>  getopenIdByOrderId(Long orderId) {
		return  xxProductDao.getopenIdByOrderId(orderId);
	}

}
