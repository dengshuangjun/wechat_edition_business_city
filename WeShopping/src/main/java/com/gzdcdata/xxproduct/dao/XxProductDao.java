package com.gzdcdata.xxproduct.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gzdcdata.xxproduct.entity.XxProduct;

public interface XxProductDao {
	/**
	 * 通过标签id查询商品信息
	 * @param id
	 * @return
	 */
	List<XxProduct> findSameByTagId(@Param("tags") Long tagsId,
			@Param("orderBy") String orderBy);
	/**
	 * 查找每日推荐商品
	 * @return
	 */
	List<XxProduct> getXxProductByPage();
	/**
	 * 通过id获取商品信息
	 * @param id
	 * @return
	 */
	XxProduct getXxProductByid(Long id);
	/**
	 * 通过分类获取商品并排序
	 * @param product_category
	 * @param orderBy
	 * @return
	 */
	List<XxProduct> listOrderBy(@Param("brandId")String brandId,
			@Param("product_category")String product_category,
			@Param("orderBy") String orderBy);
	/**
	 * 根据产品id查询订单需要的相关字段信息
	 * @param id
	 * @return
	 */
	XxProduct getOrderXxProductByid(Long id);
	/**
	 * 获取某个用户收藏的商品并排序
	 * @param memberId
	 * @param orderBy
	 * @return
	 */
	List<XxProduct> selFavoriteProductByMember(@Param("memberId")String memberId, @Param("orderBy")String orderBy);

	/**
	 * 根据订单ID
	 * @param id
	 * @return   订单商品的所属分类 的一级分类 openid
	 *           xx_product_category.openid
	 */
    List<Map<String, Object>> getopenIdByOrderId(Long _orderId);


}
