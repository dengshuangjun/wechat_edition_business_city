package com.gzdcdata.xxpayment.dao;

import com.gzdcdata.xxpayment.entity.XxPayment;

public interface XxPaymentDao {
	/**
	 * 初始化付款单信息
	 * @param xxPayment
	 */
	void insertInit(XxPayment xxPayment);

	XxPayment getByOrderId(Long orderId);

	XxPayment queryById(String id);

	XxPayment queryBySn(String paymentSn);
	/**
	 * 通过支付id更新 支付状态
	 * @param paymentId
	 * @param order_status
	 */
	void updateStatusByPaymentId(String trade_type,String paymentId, int status);
}
