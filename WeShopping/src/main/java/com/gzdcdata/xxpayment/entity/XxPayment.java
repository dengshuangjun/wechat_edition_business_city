package com.gzdcdata.xxpayment.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

/**
 * 收款单实体类
 * @author dsj
 *
 *
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxPayment {
	private Long id;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date create_date;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date modify_date;
	private String  account;
	private Double amount;
	private String bank;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date expire;
	private Double fee;
	private String memo;
	private Integer method;
	private String operator;
	private String payer;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date payment_date;
	private String payment_method;
	private String payment_plugin_id;
	private String sn;
	private Integer status;
	private Integer type;
	private Long member;
	private Long orders;
	private Long merch_product;
}
