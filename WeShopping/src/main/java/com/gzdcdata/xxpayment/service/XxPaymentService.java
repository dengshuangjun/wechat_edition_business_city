package com.gzdcdata.xxpayment.service;

import com.gzdcdata.xxpayment.entity.XxPayment;

public interface XxPaymentService {
	/**
	 * 初始化付款单信息
	 * @param xxPayment
	 */
	void insertInit(XxPayment xxPayment) throws Exception;

	XxPayment getByOrderId(Long orderId);

	/**
	 *查询付款单信息
	 * @param paymentId
	 * @return
	 */
	XxPayment queryById(String paymentId);
	/**
	 * 查询付款单信息
	 * @param paymentSn
	 * @return
	 */
	XxPayment queryBySn(String paymentSn);


	/**
	 * 更新支付状态
	 * @param paymentId
	 * @param status
	 */
	void updateStatusByPaymentId(String trade_type ,String paymentId, int status);

}
