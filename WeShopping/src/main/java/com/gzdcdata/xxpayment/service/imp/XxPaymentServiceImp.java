package com.gzdcdata.xxpayment.service.imp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.gzdcdata.xxpayment.dao.XxPaymentDao;
import com.gzdcdata.xxpayment.entity.XxPayment;
import com.gzdcdata.xxpayment.service.XxPaymentService;
/**
 * 收款单服务层
 * @author dsj
 *
 *
 */
@Service("xxPaymentService")
public class XxPaymentServiceImp implements XxPaymentService{

	@Resource(name="xxPaymentDao")
	private XxPaymentDao xxPaymentDao;
	@Override
	public void insertInit(XxPayment xxPayment)  throws Exception{
		xxPaymentDao.insertInit(xxPayment);
	}
	@Override
	public XxPayment getByOrderId(Long orderId) {
		return xxPaymentDao.getByOrderId(orderId);
	}
	@Override
	public XxPayment queryById(String id) {
		return xxPaymentDao.queryById(id);
	}
	@Override
	public XxPayment queryBySn(String paymentSn) {
		return xxPaymentDao.queryBySn(paymentSn);
	}


	@Override
	public void updateStatusByPaymentId(String trade_type,String paymentId, int status) {
		  xxPaymentDao.updateStatusByPaymentId(trade_type,paymentId,status);
	}

}
