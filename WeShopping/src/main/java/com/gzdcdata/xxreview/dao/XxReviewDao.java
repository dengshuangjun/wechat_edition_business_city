package com.gzdcdata.xxreview.dao;

import java.util.List;

import com.gzdcdata.xxreview.entity.XxReview;

public interface XxReviewDao {
	/**
	 * 一次插入多个评论信息
	 * @param list
	 */
	void insertList(List<XxReview> list);

	/**
	 * 通过产品id获取产品评论
	 * @param productId
	 */
	List<XxReview> selectreviewByProductId(String productId);

}
