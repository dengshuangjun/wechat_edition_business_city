package com.gzdcdata.xxreview.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.CusAccessObjectUtil;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.core.common.StringUtil;
import com.gzdcdata.xxreview.entity.XxReview;
import com.gzdcdata.xxreview.service.XxReviewService;

/**
 * 评论控制器
 * @author dsj
 *
 *
 */
@Controller
@RequestMapping("/xxreview")
public class XxReviewController extends ApiController{

	@Resource(name="xxReviewService")
	private  XxReviewService xxReviewService;

	@PostMapping("/subReview")
	public ResponseEntity<ResponseData> insertReview(String sn,HttpServletRequest request,
			String xxReviewsStr,String memberId){
		if(sn==null||sn.trim()==null||
				xxReviewsStr==null||memberId==null||
				xxReviewsStr.trim()==""||memberId.trim()==""
				||!StringUtil.isNumeric(memberId)){
			return new ResponseEntity<ResponseData>(fail("参数错误"),HttpStatus.BAD_REQUEST);
		}
		try {
			String ip=CusAccessObjectUtil.getIpAddress(request);
			logger.info("提交评论，用户ip:"+ip);
			logger.info("提交评论，用户id:"+memberId);
			logger.info("提交评论，评论内容json字符串"+xxReviewsStr);
			//字符串转list集合
			List<XxReview> list = JSONObject.parseArray(xxReviewsStr, XxReview.class);
			for(XxReview li:list){
				li.setIp(ip);
				li.setMember(Long.parseLong(memberId));
				li.setIs_show(true);
			}
			xxReviewService.insertList(list,sn);
			return new ResponseEntity<ResponseData>(success("评论成功!"),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("提交评论出错"+e.getMessage());
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("评论失败!"),HttpStatus.BAD_REQUEST);
	}

}
