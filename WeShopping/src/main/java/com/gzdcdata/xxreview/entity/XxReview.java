package com.gzdcdata.xxreview.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

/**
 * 评论实体
 * @author Tianqing
 *
 *
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxReview {
	private Long id;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT-8")
	private Date create_date;
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT-8")
	private Date modify_date;
	private String content;
	private String ip;
	private Boolean is_show;
	private Integer score;
	private Long member;
	private Long product;

	//非数据库字段
	private String imgStrs;
	private String userImg;//用户头像
	private String nickname;
}
