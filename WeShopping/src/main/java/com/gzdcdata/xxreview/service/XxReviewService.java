package com.gzdcdata.xxreview.service;

import java.util.List;

import com.gzdcdata.xxreview.entity.XxReview;

public interface XxReviewService {
	/**
	 * 批量插入评论信息
	 * @param list
	 * @param sn
	 */
	void insertList(List<XxReview> list, String sn) throws Exception;
	/**
	 * 分页获取产品评论信息
	 * @param productId
	 * @param page
	 * @param rows
	 * @return
	 */
	List<XxReview> selectreviewByProductId(String productId, Integer page, Integer rows);

}
