package com.gzdcdata.xxreview.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.xxorder.dao.XxOrderDao;
import com.gzdcdata.xxreview.dao.XxReviewDao;
import com.gzdcdata.xxreview.entity.XxReview;
import com.gzdcdata.xxreview.service.XxReviewService;

@Service("xxReviewService")
public class XxReviewServiceImp implements XxReviewService{

	@Resource(name="xxReviewDao")
	private XxReviewDao  xxReviewDao;
	@Resource(name="xxOrderDao")
	private XxOrderDao xxOrderDao;
	@Override
	public void insertList(List<XxReview> list,String sn) throws Exception{
		//TODO  XxReview里面的imgStrs是一个字符串的微信端图片下载地址 暂时不做操作
		xxReviewDao.insertList(list);

		xxOrderDao.updateOrderBySn(String.valueOf(list.get(0).getMember()), sn, 3);
		//更新当前订单状态
	}
	@Override
	public List<XxReview> selectreviewByProductId(String productId, Integer page, Integer rows) {
		PageHelper.startPage(page, rows);
		List<XxReview> list=xxReviewDao.selectreviewByProductId(productId);
		PageInfo<XxReview> pageInfo=new PageInfo<>(list);
		return pageInfo.getList();
	}

}
