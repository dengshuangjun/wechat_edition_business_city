package com.gzdcdata.text.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.StringUtil;
import com.gzdcdata.text.entity.Novel;
import com.gzdcdata.text.service.NovelService;

/**
 * 得成数据公众号文章控制层
 * 本类的所有方法支持跨域请求
 * @author dsj
 *
 *
 */
@Controller
@RequestMapping("/dcdata/text")
public class NovelController extends ApiController{

	@Resource(name="novelService")
	private NovelService novelService;
	/**
	 * 分页获取所有小说书名列表
	 * @return
	 */
	@GetMapping("/listTitle")
	@ResponseBody
	public ResponseEntity<String> listTitle(
			@RequestParam(name="page",defaultValue="1")  Integer page,
			@RequestParam(name="rows",defaultValue="10")  Integer rows
			,String callback ){
			PageInfo<Novel> novels=novelService.getListTitleByPage(page,rows);
		 if (StringUtil.isNotEmpty(callback )) {
			 logger.info("跨域请求");
	            //返回js文件需要的格式
			 HttpHeaders responseHeaders = new HttpHeaders();
		     responseHeaders.set("Content-Type", "text/plain;charset=utf-8");
	         return new ResponseEntity<String>((callback + "(" + JSON.toJSONString(success(novels)) + ")"),responseHeaders,HttpStatus.OK);
	        }
		return new ResponseEntity<String>(JSON.toJSONString(success(novels)),HttpStatus.OK);
	}
	/**
	 * 通过书的编号分页获取章节标题信息
	 * @param booksSn
	 * @param page
	 * @param rows
	 * @return
	 */
	@GetMapping("/listChapter")
	@ResponseBody
	public ResponseEntity<String> listChapter(String booksSn,
			@RequestParam(name="page",defaultValue="1")  Integer page,
			@RequestParam(name="rows",defaultValue="10")  Integer rows,
			String callback){
		if(booksSn==null||"".equals(booksSn)){
			return new ResponseEntity<String>(JSON.toJSONString(fail("参数错误！")),HttpStatus.BAD_REQUEST);
		}
		PageInfo<Novel> novels=novelService.getListChapterByPage(booksSn,page,rows);

		if (StringUtil.isNotEmpty(callback )) {
			 logger.info("跨域请求");
	            //返回js文件需要的格式
			 HttpHeaders responseHeaders = new HttpHeaders();
		     responseHeaders.set("Content-Type", "text/plain;charset=utf-8");
	         return new ResponseEntity<String>((callback + "(" + JSON.toJSONString(success(novels)) + ")"),responseHeaders,HttpStatus.OK);
	        }

		return new ResponseEntity<String>(JSON.toJSONString(success(novels)),HttpStatus.OK);
	}
	@GetMapping("/queryContentById")
	@ResponseBody
	public ResponseEntity<String> queryContentById(String id,String callback){
		if(id==null||"".equals(id)){
			return new ResponseEntity<String>(JSON.toJSONString(fail("参数错误！")),HttpStatus.BAD_REQUEST);
		}
		Novel novel=novelService.getContentById(id);
		if (StringUtil.isNotEmpty(callback )) {
			 logger.info("跨域请求");
	            //返回js文件需要的格式
			 HttpHeaders responseHeaders = new HttpHeaders();
		     responseHeaders.set("Content-Type", "text/plain;charset=utf-8");
	         return new ResponseEntity<String>((callback + "(" + JSON.toJSONString(success(novel)) + ")"),responseHeaders,HttpStatus.OK);
	        }
		return new ResponseEntity<String>(JSON.toJSONString(success(novel)),HttpStatus.OK);

	}

}
