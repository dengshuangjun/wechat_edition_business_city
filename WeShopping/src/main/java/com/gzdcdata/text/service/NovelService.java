package com.gzdcdata.text.service;

import com.github.pagehelper.PageInfo;
import com.gzdcdata.text.entity.Novel;

public interface NovelService {

	PageInfo<Novel> getListTitleByPage(Integer page, Integer rows);

	PageInfo<Novel> getListChapterByPage(String booksSn, Integer page, Integer rows);

	Novel getContentById(String id);

}
