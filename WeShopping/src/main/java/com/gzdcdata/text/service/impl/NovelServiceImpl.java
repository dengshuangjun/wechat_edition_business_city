package com.gzdcdata.text.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzdcdata.text.dao.NovelDao;
import com.gzdcdata.text.entity.Novel;
import com.gzdcdata.text.service.NovelService;
@Service("novelService")
public class NovelServiceImpl implements NovelService {

	@Resource(name="novelDao")
	private NovelDao novelDao;
	@Override
	public PageInfo<Novel> getListTitleByPage(Integer page, Integer rows) {
		PageHelper.startPage(page, rows);
		List<Novel> list=novelDao.listTitle();
		PageInfo<Novel> pageInfo=new PageInfo<Novel>(list);
		return pageInfo;
	}
	@Override
	public PageInfo<Novel> getListChapterByPage(String booksSn, Integer page, Integer rows) {
		PageHelper.startPage(page, rows);
		List<Novel> list=novelDao.listChapterBySn(booksSn);
		PageInfo<Novel> pageInfo=new PageInfo<Novel>(list);
		return pageInfo;
	}
	@Override
	public Novel getContentById(String id) {
		return novelDao.queryContentById(id);
	}
}
