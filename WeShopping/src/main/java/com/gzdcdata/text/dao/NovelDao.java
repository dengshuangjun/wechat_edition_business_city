package com.gzdcdata.text.dao;

import java.util.List;

import com.gzdcdata.text.entity.Novel;

public interface NovelDao {

	List<Novel> listTitle();

	List<Novel> listChapterBySn(String booksSn);

	Novel queryContentById(String id);

}
