package com.gzdcdata.text.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.ToString;
@Data
@ToString
public class Novel {

	private Long id;
	private String title;
	private String content;
	private Long orders;
	private String bookname;
	private String author;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	private Date create_date;
	private Long sn;
}
