package com.gzdcdata.xxproductcategory.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class XxProductCategory {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date create_date;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date modify_date;
	private Integer orders;
	private Integer grade;
	private String name;
	private String seo_description;
	private String seo_keywords;
	private String seo_title;
	private String tree_path;
	private Long parent;
	private String image;

}
