package com.gzdcdata.xxproductcategory.dao;

import java.util.List;

import com.gzdcdata.xxproductcategory.entity.XxProductCategory;

public interface XxProductCategoryDao {

	List<XxProductCategory> getAllProductCategoryData();

	List<XxProductCategory> getListParent();

}
