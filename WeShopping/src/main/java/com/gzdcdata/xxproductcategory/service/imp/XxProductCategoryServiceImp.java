package com.gzdcdata.xxproductcategory.service.imp;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gzdcdata.xxproductcategory.dao.XxProductCategoryDao;
import com.gzdcdata.xxproductcategory.entity.XxProductCategory;
import com.gzdcdata.xxproductcategory.service.XxProductCategoryService;
/**
 *产品分类的实现类
 * @author dsj
 *
 */
@Service("xxProductCategoryService")
public class XxProductCategoryServiceImp implements XxProductCategoryService{
	@Resource(name="xxProductCategoryDao")
	private XxProductCategoryDao xxProductCategoryDao;

	//分类规则以英文逗号隔开的字符串
	@Value("${CATEGORY_STRS}")
	private String CATEGORY_STRS;

	@Override
	public List<XxProductCategory> getAllProductCategoryData() {
		return xxProductCategoryDao.getAllProductCategoryData();
	}

	@Override
	public List<Object> getOrderByCategoreyObject () throws Exception{
		JSONArray arrs = JSONObject.parseArray(CATEGORY_STRS);
		List<Object> list=new ArrayList<Object>();
		for(Object st:arrs){
			list.add(((JSONObject)(st)).toJSONString());
		}
		return list;
	}

	@Override
	public List<XxProductCategory> getListParent() throws Exception {
		return xxProductCategoryDao.getListParent();
	}

}
