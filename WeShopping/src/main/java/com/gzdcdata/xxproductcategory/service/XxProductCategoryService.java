package com.gzdcdata.xxproductcategory.service;

import java.util.List;

import com.gzdcdata.xxproductcategory.entity.XxProductCategory;
/**
 * 产品分类
 * @author dsj
 *
 *
 */
public interface XxProductCategoryService {
	/**
	 * 获取分页或者全部商品分类信息 主要是在于xml文件中的sql语句
	 * @return
	 */
	List<XxProductCategory> getAllProductCategoryData();
	/**
	 * 获取propertites文件配置几种分类
	 * @return
	 * @throws Exception
	 */
	List<Object> getOrderByCategoreyObject () throws Exception ;
	/**
	 * 查询所有父级分类
	 * @return
	 * @throws Exception
	 */
	List<XxProductCategory> getListParent() throws Exception ;

}
