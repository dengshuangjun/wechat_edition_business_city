package com.gzdcdata.xxproductcategory.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 产品分类控制器
 * @author dsj
 *
 *
 */

import com.gzdcdata.core.common.ApiController;
import com.gzdcdata.core.common.ResponseData;
import com.gzdcdata.xxproductcategory.entity.XxProductCategory;
import com.gzdcdata.xxproductcategory.service.XxProductCategoryService;
@Controller
@RequestMapping("/xxProductCategory")
public class XxProductCategoryController  extends ApiController{
	@Resource(name="xxProductCategoryService")
	private XxProductCategoryService xxProductCategoryService;

	/**
	 * 获取默认的四种分类方式
	 */
	@RequestMapping("/listOrderByCategoreyObject")
	public ResponseEntity<ResponseData> listOrderByCategoreyObjectn(){
		try {
			List<Object> list=xxProductCategoryService.getOrderByCategoreyObject();
			if(list!=null&&list.size()>0){
				return new ResponseEntity<ResponseData>(success(list),HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("获取失败！"),HttpStatus.BAD_GATEWAY);
	}
	/**
	 * 获取所有父级分类
	 * @return
	 */
	@GetMapping("listParent")
	public ResponseEntity<ResponseData> listParent(){
		try {
			List<XxProductCategory> list=xxProductCategoryService.getListParent();
			return new ResponseEntity<ResponseData>(success(list),HttpStatus.OK);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData>(fail("获取失败！"),HttpStatus.BAD_GATEWAY);
	}
}
