<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Cache-Control" content="max-age=180">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-dns-prefetch-control" content="on">
<title>错误页面</title>
<link href="<%= this.getServletContext().getContextPath() %>/my/css/iconfont.css" rel="stylesheet" type="text/css" />
<link href="<%= this.getServletContext().getContextPath() %>/my/css/basestyle.css" rel="stylesheet" type="text/css" />
<link href="<%= this.getServletContext().getContextPath() %>/my/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%= this.getServletContext().getContextPath() %>/my/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%= this.getServletContext().getContextPath() %>/my/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath() %>/my/js/common.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath() %>/my/js/swiper.min.js"></script>
</head>
<body style="background:#fff">
  <div class="suc-pri">
    <div class="zfcg_fx"><img src="<%= this.getServletContext().getContextPath() %>/my/images/dele-icon2.png" /><span>该功能正在开发中</span></div>
    <h1>您可以返回主页看看其他商品</h1>
    <a href="<%= this.getServletContext().getContextPath() %>/index.html" class="fx_py" style="background-color: green;border-bottom: 0px;">返回首页</a>
  </div>
  <div class="suc-pro-xx">
    <div class="tab txt-center">问题咨询<em class="num">862454410</em></div>
  </div>
</body>
</html>