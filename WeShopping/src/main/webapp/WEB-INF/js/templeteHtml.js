( function(window) {
		var t = {}
		//生成首页轮播图片模版
		t.getXxAds = function() {
			var update = new StringBuilder();
			update.append('<li style="display: table-cell; vertical-align: top; width: 768px;">');
			update.append('<a href="{url}l" target="_blank">');
			update.append('<img src="{path}" alt="{title}" ppsrc="{path}">');
			update.append('	</a>');
			update.append(' </li>');
			return update.toString();
		}

		t.getXxProductCategoryTeml = function() {
			var update = new StringBuilder();
			update.append('<a href="category_list.html?id={id}" class="col-xs-3">');
			update.append('<img class="img-responsive" src="{image}">');
			update.append('<h4>{name}</h4>');
			update.append('</a>');
			return update.toString();
		}
		t.getXxTagProductHraderTeml =function (){
			var update = new StringBuilder();

			update.append('<h2 class="tab_tit">');
			update.append('<a class="more" href="my/indexMore.html?id={id}">更多</a>');
			update.append('{name}');
			update.append('</h2>');
			return update.toString();
		}

		t.getXxTagProductTeml =function (){
			var update = new StringBuilder();
			update.append('	<a class="th_link" href="views.html?id={id}">');
			update.append('<img src="{image}" style="width:100%;">');
			update.append('</a>');
			return update.toString();
		}

		t.getXxDayProductTeml =function (){
			var update = new StringBuilder();
			update.append('<a href="views.html?id={id}">');
			update.append('<div class="hproduct clearfix" style="background:#fff;border-top:0px;">');
			update.append('<div class="p-pic"><img style="max-height:100px;margin:auto;" class="img-responsive" src="{image}"></div>');
			update.append('<div class="p-info">');
			update.append('<p class="p-title">{name}</p>');
			update.append('<p class="p-origin"><em class="price">¥{price}</em>');
			update.append('</p>');
			update.append('</div>');
			update.append('</div>');
			update.append('</a>');
			return update.toString();
		}
		//view界面html片段
		t.getProductBasicInfoTeml =function (){
			var update = new StringBuilder();
			update.append('<h1 class="item-name" id="prodName">{full_name}</h1>');
			update.append('<ul>');
			update.append('<li>');
			update.append('<label>商城价格：</label>');
			update.append('<span class="price">¥<span class="price" id="prodCash">{price}</span></span>');
			update.append('</li>');
			update.append('<li>');
			update.append('<label>市场价：</label>');
			update.append('<span class="price">¥<del>{market_price}</del></span></span>');
			update.append('</li>');
			update.append('<li>');
			update.append('<label>数量：</label>');
			update.append('<div class="count_div" style="height: 30px; width: 130px;">');
			update.append('<a href="javascript:void(0);" class="minus" ></a>');
			update.append(' <input type="text" class="count" value="1" id="prodCount" readonly="readonly"/>');
			update.append('<a href="javascript:void(0);" class="add" ></a>');
			update.append('</div>');
			update.append('<a  style="float:right;" class="btn-fav" href="javascript:void(0);" onclick="addInterest(this,{id});">');
			update.append('<i class="i-fav"></i><span>收藏</span>');
			update.append('</a>');
			update.append('</li>');
			update.append('</ul>');
			return update.toString();
		}
		//分类商品列表
		t.getCategoryProductItem =function (){
			var update = new StringBuilder();
			update.append('<a href="views.html?id={id}">');
			update.append('<div class="hproduct clearfix" style="background:#fff;border-top:0px;">');
			update.append('<div class="p-pic"><img style="max-height:100px;margin:auto;" class="img-responsive" src="{image}"></div>');
			update.append('<div class="p-info">');
			update.append('<p class="p-title">{full_name}</p>');
			update.append('<p class="p-origin"><em class="price">¥{price}</em>');
			update.append('</p>');
			update.append('<p class="mb0"><del class="old-price">¥{market_price}</del></p>');
			update.append('</div>');
			update.append('</div>');
			update.append('</a>');
			return update.toString();
		}
		//生活分类标签模板
		t.getCaterory= function(){
			var update = new StringBuilder();
			update.append('<li>');
			update.append('<a href="category_list_more.html?id={id}">');
			update.append('<img alt="{name}" style="width:initial;height:100px;" src="{image}" />');
			update.append('<div style="width:90%;white-space: nowrap;text-overflow: ellipsis;overflow:hidden;text-align:center;margin: auto;">{name}</div>');
			update.append('</a>');
			update.append('</li>');
			return update.toString();
		}
		//品牌分类标签模板
		t.getBrandCaterory= function(){
			var update = new StringBuilder();
			update.append('<li>');
			update.append('<a href="category_list.html?brandId={id}">');
			update.append('<img alt="图片大小为200*105" src="{logo}" style="height: 39px;" />');
			update.append('</a>');
			update.append('</li>');
			return update.toString();
		}
		//购物车信息模板
		t.getShopCartTeml= function(){

			id:425
			image:"http://pos.gzdcdata.com/shopDc/upload/image/20180130/652b6b7e-5493-4b0d-a1a0-820813d84ffd-thumbnail.jpg"
			name:"新手机上市 vivo Y75全面屏s vivoy75手机y75a vivox20 x7x9 y77"
			price:"1,699.80"
			quantity:2

			var update = new StringBuilder();
			update.append('<ul class="list-group"><li class="list-group-item text-primary">');
			update.append('<div class="icheck pull-left mr5">');
            update.append('<input type="checkbox" checked="checked" class="ids" prodStatus="1"  itemkey="{id}"/>');
    	    update.append('<label class="checkLabel">');
            update.append('<span></span>');
            update.append('</label>');
            update.append('</div>选中购买');
            update.append('<a class="close p-close" onclick="deleteShopCart(\'{id}\',\'{name}\')" href="javascript:void(0);">×</a>');
            update.append('</li>');
            update.append('<li class="list-group-item hproduct1 clearfix">');
            update.append('<div class="p-pic1"><a href="views.html?id={id}"><img class="img-responsive" src="{image}"></a></div>');
            update.append('<div class="p-info">');
            update.append('<a href="views.html?id={id}"><p class="p-title">{name}</p></a>');
/*            update.append('<p class="p-attr">');
    		update.append('<span>颜色：红色；</span></p>');*/
    		update.append('<p class="p-origin">');
        	update.append('<div class="pull-left mt5">');
        	update.append('<span class="gary">单价：</span>');
        	update.append('<em class="red productTotalPrice">¥{price}</em>');
        	update.append('</div>');
            update.append('<div class="btn-group btn-group-sm control-num">');
			update.append('<a onclick="disDe(this)" href="javascript:void(0);" class="btn btn-default"><span class="glyphicon glyphicon-minus gary"></span></a>');
            update.append('<input type="tel" class="btn gary2 Amount" readonly="readonly" value="{quantity}" maxlength="4" itemkey="" prodId="{id}" skuId="1358">');
            update.append('<a onclick="increase(this)" href="javascript:void(0);" class="btn btn-default"><span class="glyphicon glyphicon-plus gary"></span></a>');
            update.append('</div>');
            update.append('</p>');
            update.append('</div>');
            update.append('</li></ul>');
			return update.toString();
		}
		//购物车为空时显示信息
		t.getShopCartNullTemp= function(){
			var update = new StringBuilder();
			update.append('<div id="ajax_loading" style="margin:20rem 30%;text-align:center;">');
			update.append('<div style="text-align: center;">购物车空空如也!</div>');
			update.append('</div>');
			 return update.toString();
		}
		//订单确定页面的商品列表模版
		t.getOrderProductTeml= function(){
			var update = new StringBuilder();
			update.append('<li class="list-group-item hproduct1 clearfix">');
			update.append('<div class="p-pic1"><a href="views.html?id={id}"><img class="img-responsive" src="{image}"></a></div>');
			update.append('<div class="p-info">');
			update.append('<a href="views.html?id={id}"><p class="p-title">{name}</p></a>');
			/*update.append('<p class="p-attr">');
			update.append('<span>颜色：红色；</span></p>');*/
			update.append('<p class="p-origin">');
			update.append('<em class="price">¥{price}</em><span style="float: right; margin-right: 50px;">x{quantity}</span>');
			update.append('</p>');
			update.append('</div>');
			update.append('</li>');
			update.append('<li class="list-group-item clearfix">');
			update.append('<div class="pull-left ">配送费：</div>');
			update.append('<div class="pull-right">￥{itemExpressMoney}</div>');
			update.append('</li>');
			update.append('<li class="list-group-item clearfix">');
			update.append('<div class="pull-left ">配送方式：</div>');
        	update.append('<div class="pull-right">普通快递</div>');
        	update.append('</li>');
			return update.toString();
		}
		//订单确认页面总结那一段的html模板
		t.getOrderTotalInfoTeml= function(){
			var update = new StringBuilder();
			update.append('<li class="list-group-item clearfix">');
			update.append('<div class="pull-left ">配送费总计：</div>');
        	update.append('<div class="pull-right">￥{expressTotalMoney}</div>');
        	update.append('</li>');
        	update.append(' <li class="list-group-item clearfix">');
        	update.append('<div class="pull-right">共{totalQuantity}件商品 ');
        	update.append('<span class="gary">小计：</span>');
        	update.append('<em class="red productTotalPrice">¥{productTotalMoney}</em></div>');
            update.append('</li>');
			return update.toString();
		}
		t.getAddressInfoTeml= function(){
			var update = new StringBuilder();
			update.append('<li>');
			update.append('<a class="btn list-group-item  clearfix " style="text-align: left; white-space: normal;" href="./addressList.html">');
			update.append('<div class="p-addr">');
			update.append('<div class="pull-left">收货人：{consignee} </div>');
			update.append('<div class="pull-right ">{phone} </div>');
			update.append('</div>');
			update.append('<div class="mt5">');
			update.append('<div class="pull-left" style="width: 90%">{area_name},{address} </div>');
			update.append('<div class="pull-right ">》</div>');
			update.append('</div>');
			update.append('</a>');
         	update.append('</li>');
         	return update.toString();
		}
		//地址列表
		t.getAddAddressInfoTeml= function(){
			var update = new StringBuilder();
			update.append('<li>');
			update.append('<a class="btn list-group-item  clearfix " style="text-align: left; white-space: normal;" href="./cartToOrder.html?addrId={id}">');
			update.append('<div class="p-addr">');
			update.append('<div class="pull-left">收货人：{consignee} </div>');
			update.append('<div class="pull-right ">{phone} </div>');
			update.append('</div>');
			update.append('<div class="mt5">');
			update.append('<div class="pull-left" style="width: 90%">{area_name},{address} </div>');
			update.append('<div class="pull-right ">》</div>');
			update.append('</div>');
			update.append('</a>');
         	update.append('</li>');
         	return update.toString();
		}
		//首页更多连接按标签tags查询的商品列表、
		t.getProductByTagsTeml= function(){
			var update = new StringBuilder();
			update.append('<div class="fbh-pro-list">');
			update.append('<div class="fbh-pro">');
	        update.append('<div class="fbh-pro-img"><a href="../views.html?id={id}"><img src="{image}" /></a></div>');
            update.append('<div class="fbh-pro-pri txt-yel"><span class="yahei">￥</span>{price}</div>');
            update.append('<div class="fbh-pro-tit">{full_name}</div>');
            update.append('</div>');
	        update.append('</div>');
	    	return update.toString();
		}
		//myorder展示页面通用模板头
		t.getOrderHeader = function(){
			var update = new StringBuilder();
			update.append('<div class="orderListBox">');
			update.append('<div class="order-tab">');
			update.append('<div class="odr-con"  ><div class="grey">订单号：<em  class="num green">{sn}</em></div></div>');
			update.append('<a href=\'orderDetail.html?sn={sn}\' target="_blank">');
			update.append('<div>');//onclick="javascript:window.location.href=\'orderDetail.html?sn={sn}\'"
			return update.toString();

		}
		//myorder展示页面通用模板商品列表
		t.getOrderBody =function(){
			var update = new StringBuilder();
			update.append('<div class="odr-con" style="border-bottom: 1px solid #f0f0f0">');
		    update.append('<div class="odrprolist-img"><img src="{thumbnail}"></div>');
		    update.append('<div class="odrprolist-txt">');
		    update.append('<h3>{name}</h3>');
            update.append('<div class="tab odr-info">');
            update.append('<div class="fr grey">x<em  class="num green">{quantity}</em></div>');
            update.append('<div class="listpri red fl">￥<em class="num">{price}</em></div>');
            update.append('</div>');
		    update.append('</div>');
		    update.append('</div>');
		    return update.toString();
		}
		//待付款订单尾部按钮模板
		t.getOrderFooter0Temp=function(){
			var update = new StringBuilder();
			update.append('</div>');
			update.append('</a>');
		    update.append(' <div class="odr-num">');
		    update.append('<div class="num-pri-order">');
		    update.append('<div class="odrpri">总价<span>￥<em>{amount}(含运费：￥{freight})</em></span></div>');
		    update.append('</div>');
		    update.append('<div class="opt-btn-order">');
		    update.append('<a href="javascript:void(0);" class="btn_orderlist_2" onclick="nowPay(this,\'{sn}\')">现在付款</a>');
		    update.append('<a href="javascript:void(0);" class="btn_orderlist_1" onclick="cancelOrder(this,\'{sn}\')">取消订单</a>');
		    update.append('</div>');
		    update.append('</div>');
		    update.append('</div>');
		    update.append('</div>');
			return update.toString();
		}
		//待收货订单尾部按钮模板
		t.getOrderFooter1Temp=function(){
		var update = new StringBuilder();
		update.append(' </div>');
	        update.append('<div class="odr-num">');
	        update.append('<div class="num-pri-order">');
	        update.append('<div class="odrpri">总价<span>￥<em>{amount}(含运费：￥0.00)</em></span></div>');
	        update.append('</div>');
	        update.append('<div class="opt-btn-order">');
	        update.append('<a href="javascript:void(0);"onclick="buyAgain(this,\'{sn}\')" class="btn_orderlist_2">再次购买</a>');
	        update.append('<a href="javascript:void(0);" onclick="acceptOk(this,\'{sn}\')" class="btn_orderlist_1">确认收货</a>');
	        update.append('</div>');
	        update.append('</div>');
	        update.append('</div>');
	        update.append('</div>');
	        return update.toString();
		}
		//待评价订单尾部按钮模板
		t.getOrderFooter2Temp=function(){
		    var update = new StringBuilder();
		    update.append(' </div>');
	        update.append('<div class="odr-num">');
	        update.append('<div class="num-pri-order">');
            update.append('<div class="odrpri">总价<span>￥<em>1000(含运费：￥0.00)</em></span></div>');
            update.append('</div>');
            update.append('<div class="opt-btn-order">');
            update.append('<a href="javascript:void(0);" onclick="buyAgain(this,\'{sn}\')" class="btn_orderlist_2">再次购买</a>');
            update.append('<a href="javascript:void(0);" onclick="pingjia(this,\'{sn}\')" class="btn_orderlist_1">现在评价</a>');
            update.append(' </div>');
            update.append('</div>');
	        update.append('</div>');
	        update.append('</div>');
	        return update.toString();
		}
		//已完成订单尾部按钮模板
		t.getOrderFooter3Temp=function(){
			var update = new StringBuilder();
			update.append(' </div>');
    	    update.append('<div class="odr-num">');
    	    update.append('<div class="num-pri-order">');
    	    update.append(' <div class="odrpri">总价<span>￥<em>1000(含运费：￥0.00)</em></span></div>');
    	    update.append(' </div>');
    	    update.append('<div class="opt-btn-order">');
    	    update.append('<a href="javascript:;" class="btn_orderlist_2">再次购买</a>');
    	    update.append('</div>');
    	    update.append('</div>');
    	    update.append('</div>');
    	    update.append('</div>');
		    return update.toString();
		}
		//地址管理页面
		t.getAddressMgItem=function(){
			var update = new StringBuilder();
    	    update.append('<div class="order-tab">');
    	    update.append('<div class="odr-con">');
    	    update.append('<div class="addrss-list-txt">');
    	    update.append('<div class="adr-name-tel"><span>{consignee}</span><span>{phone}</span></div>');
    	    update.append('<h3>{area_name}{address}</h3>');
    	    update.append('</div>');
		    update.append('</div>');
		    update.append('<div class="odr-num">');
		    update.append('<div class="fl shuliang adr-mr-opt"><input type="checkbox" {is_default}/><span class="shezhi-addres moren">默认地址</span></div>');
		    update.append('<div class="fr odrpri adr-txt-opt"><a href="#" onclick="del({id})">删除</a></div>');
		    update.append('</div>');
		    update.append('</div>');
		    return update.toString();
		}
		//交易关闭
		t.getOrderDetailClose = function (){
			var update = new StringBuilder();
			update.append('<div style="background-color: #eea72b;height: 4rem ;padding:1rem;">');
			update.append('<div style="font-size: 1.8rem"><span>交易关闭</span></div>');
			update.append('<div><span>超时关闭</span></div>');
			update.append('</div>');
			return update.toString();
		}
		//订单详情页面待支付提示模块
		t.getOrderDetailLead = function (){
			var update = new StringBuilder();
			update.append('<div style="background-color: #eea72b;height: 4rem ;padding:1rem;">');
			update.append('<div style="font-size: 1.8rem"><span>等待买家付款</span></div>');
			update.append('<div><span>剩{expandTime}将自动关闭</span></div>');
			update.append('</div>');
			return update.toString();
		}
		//发货中
		t.getOrderDetailLeading = function (){
			var update = new StringBuilder();
			update.append('<div style="background-color: #eea72b;height: 4rem ;padding:1rem;">');
			update.append('<div style="font-size: 1.8rem"><span>发货中...</span></div>');
			update.append('<div><span>快马加鞭发货中，请耐心等待</span></div>');
			update.append('</div>');
			return update.toString();
		}
		//交易成功
		t.getOrderDetailSuccess = function (){
			var update = new StringBuilder();
			update.append('<div style="background-color: #eea72b;height: 4rem ;padding:1rem;">');
			update.append('<div style="font-size: 1.8rem;line-height: 4rem"><span>交易成功</span></div>');
			update.append('</div>');
			return update.toString();
		}
		//订单详情界面地址模块
		t.getOrderDetailAddr= function(){
			var update = new StringBuilder();
			update.append('<div class="new-ord-address">');
		    update.append('<div class="add-tab left"><div class="icon-address"><i class="icon iconfont">&#xe619;</i></div></div>');
		    update.append('<div class="address-info">');
		    update.append('<div class="txt">');
		    update.append('<p><span>{consignee}</span><span>{phone}</span></p>');
		    update.append('<p>{area_name}{address}</p>');
		    update.append('</div>');
		    update.append('</div>');
		    update.append('</div>');
		    return update.toString();
		}
		//订单详情界面产品模块
		t.getOrderDetailProduct= function(){
			var update = new StringBuilder();
			update.append('<div class="new-ord-info">');
		    update.append('<div class="ord-pro">');
		    update.append('<div class="pro-img"><a href="#"><img src="{thumbnail}" /></a></div>');
		    update.append('<div class="pro-info">');
		    update.append('<div class="pro-name"><a href="../views.html?id={product}">{full_name}</a></div>');
		    update.append('<div class="pro-pri">￥<span>{price}</span></div>');
		    update.append('<div class="pro-day">');
		    update.append('<span>购买数量：{quantity}</span>');
			update.append('</div>');
			update.append('</div>');
		    update.append(' </div>');
		    update.append('</div>');
		    return update.toString();
		}
		//订单详情统计分类价格模块
		t.getOrderDetailCateMy=function (){
			var update = new StringBuilder();
			update.append('<div class="pri-ord-info">');
			update.append('<span>商品价格:<em>{total}</em>元</span>');
			update.append('<span>运费:<em>{freight}</em>元</span>');
			update.append('</div>');
			return update.toString();
		}
		//留言板块
		t.getOrderDetailMemo=function (){
			var update = new StringBuilder();
			update.append('<div class="new-ord-feed">');
			update.append('<span>给卖家留言</span>');
		    update.append('<div class="liuyan">');
		    update.append('<textarea disabled="disabled">{memo}</textarea>');
		    update.append('</div>');///*<i>0/140</i>*/
		    update.append('</div>');
		    return update.toString();
		}
		//结尾订单号，时间等模块
		t.getOrderDetailEnd=function (){
			var update = new StringBuilder();
			update.append('<div class="new-ord-pro-num tab">');
			update.append('<span>订单号：{orderSn}</span>');
		    update.append('</div>');
			update.append(' <div class="new-ord-pro-num tab">');
			update.append('<span>交易流水号：{xxPaySn}</span></div>');
			update.append('<div class="grey new-ord-feed">');
			update.append('创建时间：<em class="num green">{create_date}</em><br/>');
			update.append('过期时间：<em class="num green">{expire} </em>');
			update.append('</div>');
			return update.toString();
		}
		//订单详情底部付款模块
		t.getOrderDetailPay=function (){
			var update = new StringBuilder();
			update.append('<div class="pay"><span>总价:</span><em>{totalMoney}</em>元</div>');
			update.append('<a href="javascript:void(0)" class="confirm-btn" onClick="payMoney(\'{xxPaySn}\')">确认付款</a>');
		    return update.toString();
		}
		//订单详情底部确认收货模块
		t.getOrderDetailConfirmOrder=function (){
			var update = new StringBuilder();
			update.append('<div class="pay"><span>总价:</span><em>{totalMoney}</em>元</div>');
			update.append('<a href="javascript:void(0)" class="confirm-btn" onClick="confirmOrder(\'{payMentId}\')">确认收货</a>');
		    return update.toString();
		}
		//评价
		t.getOrderDetailPingjiaOrder=function (){
			var update = new StringBuilder();
			update.append('<div class="pay"><span>总价:</span><em>{totalMoney}</em>元</div>');
			update.append('<a href="javascript:void(0)" class="confirm-btn" onClick="PingjiaOrder(\'{payMentId}\')">评价</a>');
		    return update.toString();
		}
		//
		t.getOrderDetailSuccessOrder=function (){
			var update = new StringBuilder();
			update.append('<div class="pay"><span>总价:</span><em>{totalMoney}</em>元</div>');
			update.append('<a href="javascript:void(0)" class="confirm-btn" onClick="successOrder(\'{payMentId}\')">再次购买</a>');
		    return update.toString();
		}
		//评价模板
		t.getOrderPingjia=function (){
			 var update = new StringBuilder();
			  update.append('<div class="commentBox">');
			  update.append('<input type="hidden" name="productId" value="{product}"/>');
			  update.append('<div class="commentHeader" >');
			  update.append('<div class="commentPic" ><img src="{thumbnail}"/></div>');
			  update.append('<div class="commentCon" >');
			  update.append('<div style="float: left;"><span>评价等级:</span></div>');
			  update.append('<div>');
			  update.append('<input name="step" class="rating-input" type="text"value="5"/>');
			  update.append('</div>');
			  update.append('</div>');
			  update.append('</div>');
			  update.append('<div class="comment-word">');
			  update.append('<div><textarea name="memo" class="comment-ipt" placeholder="谈谈宝贝的优点和缺点吧...."></textarea></div>');
/*			  update.append('<div class="com-btn tab"><a href="javascript:;" class="tj">提交评论</a></div>');
*/			  update.append('</div>');
			  update.append('<div class="addImg" >');
			  update.append('<input type="hidden" name="imgStr"/>');
			  update.append('<a href="javascript:;" onclick="choosePhoto(this);">+添加照片</a>');
			  update.append('</div>');
			  update.append('</div>');
			  return update.toString();
		}
		//收藏列表模板
		t.getShoucangItem=function (){
			 var update = new StringBuilder();
			 update.append('<li style="position: relative;">');
		     update.append('<!-- <div class="checkBox" style="width: 100%">');
			 update.append('</div>	 -->');
			 update.append('<a href="../views.html?id={id}">');
			 update.append('<img alt="图片大小为100*100" style="width: 100%" src="{image}" >');
			 update.append('<div>{name}</div>');
			 update.append('<em class="price">¥{price}</em>');
			 update.append('</a>');
			 update.append('</li>');
		     return update.toString();
		 }
		window.templete = t;
	}(window));