/**
 * 地址栏参数解析
 * @param url window.location.search即可
 * @returns
 */
function parseURL(url){
	var url = url.split("?")[1];
    var res = {};
    var arr = [];
    if(url ==undefined){
	return res ;
       }
    var para = url.split("&");
    var len = para.length;

    for(var i=0;i<len;i++){
       arr = para[i].split("=");
       res[arr[0]] = arr[1];
     }
    return res;
}
/**
 * 获取项目路径
 */
function getRealPath(){
	  //获取当前网址，如： http://localhost:8083/myproj/view/my.jsp
	   var curWwwPath=window.location.href;
	   //获取主机地址之后的目录，如： myproj/view/my.jsp
	  var pathName=window.location.pathname;
	  var pos=curWwwPath.indexOf(pathName);
	  //获取主机地址，如： http://localhost:8083
	  var localhostPaht=curWwwPath.substring(0,pos);
	  //获取带"/"的项目名，如：/myproj
	  var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);

	 //得到了 http://localhost:8083/myproj
	  var realPath=localhostPaht+projectName;
	  return realPath;
}