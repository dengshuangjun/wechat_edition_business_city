function loginInfo(backLogin){
	var loginInfo=jQuery.getCookie("loginInfo");
	if(loginInfo==""||loginInfo==undefined){//没有登录 申请授权并登录
		//获取授权code然后异步请求
		var redirect_uri= window.location.href;
		var code=parseURL(window.location.search).code;
		if(code!=null&&code!=""&&code!=undefined){//通过code异步获取用户信息
			//console.log(JSON.stringify(ret.data));
			floatNotify.simple("登录中！");
				$.get(getRealPath()+"/home/login",{code:code},function(ret){
					if(ret&&ret.data){
						jQuery.setCookie("loginInfo",JSON.stringify(ret.data),30);
						typeof backLogin == "function" && backLogin(ret.data);
						floatNotify.simple("登录成功！");
					}else{
						typeof backLogin == "function" && backLogin(undefined);
						floatNotify.simple("登录失败！");
					}
				},'json');
		    }else{//授权页面弹出之后再重新进入本页面获取用户信息存入cookie
		    	//var temp={amount:"0.000000000000",balance:"0.000000000000",email:"",gender:1,id:16702,is_enabled:true,login_date:"2018-04-20",
		    			//login_ip:"192.168.99.100",member_rank:1,name:"",nick_name:"苏海峰",username:"oIOH5v2ZC2jlyQcXJUNzQuUGbtpc",
		    			//attribute_value0:"https://wx.qlogo.cn/mmopen/vi_32/gfFibNDRckbho9NUgCHznnn8tSkEptaUQC6I9VLaUia1X3tMMyNjMscuIXsWk4GcibF8a4BRj01BDJX75oNwlpicJg/0"};
		    	//jQuery.setCookie("loginInfo",JSON.stringify(temp),30);
		    	window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?" +
		    	"appid=wx964a7f6d96826955&redirect_uri="+redirect_uri+"&response_type=code&scope=snsapi_userinfo&state=snsapi_userinfo#wechat_redirect"
			}
	}else{
		//console.log(loginInfo);
		  typeof backLogin == "function" && backLogin(JSON.parse(loginInfo));
	}
}
/*loginInfo(function(userInfo){
	console.log(userInfo);
})*/