var page=1;//页数
var rows=10;//行数
var categoryId="";//分类id
var orderBy="";//分类规则
var brandId=""//品牌id
jQuery(document).ready(function() {
	categoryId=parseURL(window.location.search).id;
	brandId=parseURL(window.location.search).brandId;
	sendData();//一过来就调用
	$(".fanhui_cou").click(function(){
		$("body,html").animate({scrollTop:0},200);
		return false;
	});
	$(window).scroll(function(){
        if ($(document).scrollTop() + $(window).height() == $(document).height()){
        	appendData();
        }
    });
});
function appendData(){
	$('#ajax_loading div').html("玩命加载中...");
	page=parseInt(page)+1;
	$.get(getRealPath()+"/product/listOrderBy",{orderBy:orderBy,page:page,rows:rows,product_category:categoryId,brandId:brandId},function(ret){
		if(ret&&ret.code==200&&ret.data.list.length>0){
			var list= ret.data.list;
			for(var i=0;i<list.length;i++){
				$(".productData").append(String.formatmodel(templete.getCategoryProductItem(),list[i]));
			}
		}else{
			$('#ajax_loading div').html("小成正在努力上架中,敬请期待");
		}
	},'json')
}

//加载初始菜单数据
function sendData(){
	$.get(getRealPath()+"/xxProductCategory/listOrderByCategoreyObject",{},function(ret){
		if(ret&&ret.code==200){
			var datas=ret.data;
			var categoryOrderBy="";
			for(var i=0;i<datas.length;i++){
				var datas1=eval('('+ datas[i]+ ')');
				if(i==0){
					categoryOrderBy+='<li id="'+datas1.orderBy+'" class="active">';
					orderBy=datas1.orderBy+" desc";
				}else{
					categoryOrderBy+='<li id="'+datas1.orderBy+'" >';

				}
				categoryOrderBy+='<a title="'+datas1.name+'"  href="javascript:void(0);">'+datas1.name;
				if(i!=0){
					categoryOrderBy+='<i class="icon_sort"></i>';
				}
				categoryOrderBy+='</a></li>';
			}
			categoryOrderBy+='<span id="category"><a title="" href="javascript:void(0);"><em class="icon_sort_category"></em></a></span>';
			$("#categoryOrderBy").html(categoryOrderBy);
			categoryClick();
			initProductData();//默认为空排序
			if ($(document).scrollTop() + $(window).height() <= $(document).height()){
	        	appendData();
	        }
		}else{
			floatNotify.simple("加载失败！");
		}
	},'json')
}
function initProductData(){
	$(".productData").html('<a href="#"><img style="height: 25px; width: 25px;margin: 20rem 48%;" src="images/loading.gif" alt="加载中"></a>');
	$.get(getRealPath()+"/product/listOrderBy",{orderBy:orderBy,page:page,rows:rows,product_category:categoryId,brandId:brandId},function(ret){
		if(ret&&ret.code==200){
			var list= ret.data.list;
			if(list.length==0){
				$('#ajax_loading div').html("小成正在努力上架中,敬请期待!");
			}
			var xxadsListsHtml="";
			for(var i=0;i<list.length;i++){
				xxadsListsHtml+=String.formatmodel(templete.getCategoryProductItem(),list[i]);
			}
			$(".productData").html(xxadsListsHtml);

		}else{
			floatNotify.simple("获取数据失败！");
		}
	},'json')
}
//绑定 点击事件
function categoryClick(){
	$(".row ul span").bind("click",function() {
		$(".row ul li").each(function(i) {
				$(this).removeClass("active");
		});
		$(this).addClass("active");
	});
	$(".row ul li").bind("click",function() {
		var id = $(this).attr("id");
		$(".row ul span").each(function(i) {
			$(this).removeClass("active");
		});
		//var orderDir = "";;
		$(".row ul li").each(function(i) {
			if (id != $(this).attr("id")) {
				$(this).removeClass("active");
			}
		});

		$(this).addClass("active");
		var iElement=$(this).find("i");
		page=1;
			if ($(iElement).hasClass("icon_sort_up")) {
				orderBy = id+" asc";
				$(iElement).attr("class","icon_sort_down");

			} else if($(iElement).hasClass("icon_sort_down")){
				orderBy = id+" desc";
				$(iElement).attr("class","icon_sort_up");

			}else{
				orderBy = id+" desc";
				$(iElement).attr("class","icon_sort_up");
			}

		$(this).siblings().find("i").attr("class","icon_sort");

		var no_results=$.trim($("#no_results").html());
		if(no_results!="" && no_results!=null && no_results!=undefined){
			return false;
		}
		initProductData();
	});
}
/**判断是否为空**/
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}