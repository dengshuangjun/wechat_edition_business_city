/**
 * 收藏页面
 *
 */
var page=1;//页数
var rows=10;//行数
var orderBy="";//分类规则
jQuery(document).ready(function() {
	//加载分类  初始页数据
	sendData();
	$(window).scroll(function(){
        if ($(document).scrollTop() + $(window).height() == $(document).height()){
        	appendData();
        }
    });
});
//筛选排序规则初始化
function sendData(){
	$.ajax({
		url: getRealPath()+"/xxProductCategory/listOrderByCategoreyObject",
		data: {},
		type:'get',
		async : true, //默认为true 异步
		error:function(data){
			floatNotify.simple("加载失败！请重试");
		},
		success:function(ret){
			var datas=ret.data;
			var categoryOrderBy="";
			for(var i=0;i<datas.length;i++){
				var datas1=eval('('+ datas[i]+ ')');
				if(i==0){
					categoryOrderBy+='<li id="'+datas1.orderBy+'" class="active">';
					orderBy=datas1.orderBy+" desc";
				}else{
					categoryOrderBy+='<li id="'+datas1.orderBy+'" >';

				}
				categoryOrderBy+='<a title="'+datas1.name+'"  href="javascript:void(0);">'+datas1.name;
				if(i!=0){
					categoryOrderBy+='<i class="icon_sort"></i>';
				}
				categoryOrderBy+='</a></li>';
			}
			categoryOrderBy+='<span id="category"><a title="" href="javascript:void(0);"><em class="icon_sort_category"></em></a></span>';
			$("#categoryOrderBy").html(categoryOrderBy);
			categoryClick();
		}
	});
}
//绑定 点击事件
function categoryClick(){
	page=1;//页数
	rows=10;//行数
	$(".row ul span").bind("click",function() {
		$(".row ul li").each(function(i) {
				$(this).removeClass("active");
		});
		$(this).addClass("active");
	});
	$(".row ul li").bind("click",function() {
		var id = $(this).attr("id");
		$(".row ul span").each(function(i) {
			$(this).removeClass("active");
		});
		$(".row ul li").each(function(i) {
			if (id != $(this).attr("id")) {
				$(this).removeClass("active");
			}
		});
		$(this).addClass("active");
		var iElement=$(this).find("i");
		page=1;
			if ($(iElement).hasClass("icon_sort_up")) {
				orderBy = id+" asc";
				$(iElement).attr("class","icon_sort_down");

			} else if($(iElement).hasClass("icon_sort_down")){
				orderBy = id+" desc";
				$(iElement).attr("class","icon_sort_up");

			}else{
				orderBy = id+" desc";
				$(iElement).attr("class","icon_sort_up");
			}

		$(this).siblings().find("i").attr("class","icon_sort");

		var no_results=$.trim($("#no_results").html());
		if(no_results!="" && no_results!=null && no_results!=undefined){
			return false;
		}
		initSCProductData();
	});
	initSCProductData();
}
/**
 * 默认排序方式加载收藏商品
 */
function initSCProductData(){
	loginInfo(function(userInfo){
		$.ajax({
			url: getRealPath()+"/xxmember/selFavoriteByMember",
			data: {memberId:userInfo.id,page:page,rows:rows,orderBy:orderBy},
			type:'post',
			async : true, //默认为true 异步
			error:function(data){
				$("#shouCangList").html('<li style="width: 100%;"><div  style="text-align: center;padding: 21rem 0;">加载失败!请刷新重试</div></li>');
			},
			success:function(data){
				page+=1;
				if(data&&data.code==200&&data.data){
					$("#shouCangList").html('');
					var datas=data.data;console.log(datas);
					for(var i=0; i<datas.length;i++){
						$("#shouCangList").append(String.formatmodel(templete.getShoucangItem(),datas[i]));
					}
					if ($(document).scrollTop() + $(window).height() <= $(document).height()){
						appendData();
			        }
				}
			}
		});
	})
}

function appendData(){
	$("#leadingMsg").html("加载中!");
	loginInfo(function(userInfo){
		$.ajax({
			url: getRealPath()+"/xxmember/selFavoriteByMember",
			data: {memberId:userInfo.id,page:page,rows:rows,orderBy:orderBy},
			type:'post',
			async : true, //默认为true 异步
			error:function(data){
				$("#shouCangList").html('<li style="width: 100%;"><div  style="text-align: center;padding: 21rem 0;">加载失败!请刷新重试</div></li>');
			},
			success:function(data){
				page+=1;
				if(data&&data.code==200&&data.data.length>0){
					var datas=data.data;
					for(var i=0; i<datas.length;i++){
						$("#shouCangList").append(String.formatmodel(templete.getShoucangItem(),datas[i]));
					}
				}else{
					$("#leadingMsg").html("没有更多收藏信息了!");
				}
			}
		});
	})
}