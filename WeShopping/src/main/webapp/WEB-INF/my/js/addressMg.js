$(function(){
	//加载地址列表
	 loadAddressList();

})

function loadAddressList(){
	loginInfo(function(userInfo){
		$.get(getRealPath()+"/xxreceiver/listByMember",{memberId:userInfo.id},function(ret){
			if(ret&&ret.data&&ret.data.length>0){
				var datas=ret.data;
				$(".orderListBox").html('');
				for(var i in datas){
					if(datas[i].is_default){
						datas[i].is_default="checked='checked'";
					}else{
						datas[i].is_default="";
					}
					$(".orderListBox").append(String.formatmodel(templete.getAddressMgItem(), datas[i]));
				}
			}else{
				$(".orderListBox").html('<div id="ajax_loading" style="width:300px;margin:10px auto 15px;text-align:center;"><div style="text-align: center;">暂无地址信息!</div></div>');
			}
		},'json');

	})
}
function del(id){
	$("#iosDialog1").attr("style","opacity:1;display:block;")
	$($("#iosDialog1 a")[0]).click(function(){
		$("#iosDialog1").attr("style","opacity:0;display:none;");
		$($("#iosDialog1 a")[0]).unbind();
		$($("#iosDialog1 a")[1]).unbind();
	})
	$($("#iosDialog1 a")[1]).click(function(){
		$("#iosDialog1").attr("style","opacity:0;display:none;");
		$("#loadingToast").attr("style","opacity:1;display:block;");
		$.get("../xxreceiver/del",{id:id},function(ret){
			$("#loadingToast").attr("style","opacity:0;display:none;");
			if(ret&&ret.code==200){
				floatNotify.simple("删除成功!");
				loadAddressList();
			}else{
				floatNotify.simple("删除失败!");
				loadAddressList();
			}
		},'json');
		$($("#iosDialog1 a")[0]).unbind();
		$($("#iosDialog1 a")[1]).unbind();
	})
}
